﻿Public Class WW_MainfreightDetail_Custom
    Private dispatchnotestring As String
    Private consignmentnumberstring As String
    Private sysprosalesorderstring As String
    Private detailidint As Integer
    Private itemsdec As Decimal
    Private descriptionstring As String
    Private cubicvaluedec As Decimal
    Private cubicunitstring As String
    Private weightvaluedec As Decimal
    Private weightunitstring As String
    Private dimensionquantitydec As Decimal
    Private dimensionlengthdec As Decimal
    Private dimensionwidthdec As Decimal
    Private dimensionheightdec As Decimal


    Public Property ConsignmentNo As String
        Get
            Return consignmentnumberstring

        End Get
        Set(value As String)
            consignmentnumberstring = value
        End Set
    End Property

    Public Property DetailID As Integer

        Get
            Return detailidint

        End Get
        Set(value As Integer)
            detailidint = value
        End Set
    End Property

    Public Property SysproDispatchNote As String
        Get
            Return dispatchnotestring

        End Get
        Set(value As String)
            dispatchnotestring = value
        End Set
    End Property



    Public Property SYSPROSalesOrder As String
        Get
            Return sysprosalesorderstring

        End Get
        Set(value As String)
            sysprosalesorderstring = value
        End Set
    End Property

    Public Property Items As Decimal
        Get
            Return itemsdec

        End Get
        Set(value As Decimal)
            itemsdec = value
        End Set
    End Property

    Public Property Description As String
        Get
            Return descriptionstring

        End Get
        Set(value As String)
            descriptionstring = value
        End Set
    End Property


    Public Property CubicValue As Decimal
        Get
            Return cubicvaluedec

        End Get
        Set(value As Decimal)
            cubicvaluedec = value
        End Set
    End Property

    Public Property CubicUnit As String
        Get
            Return cubicunitstring

        End Get
        Set(value As String)
            cubicunitstring = value
        End Set
    End Property

    Public Property WeightValue As Decimal
        Get
            Return weightvaluedec

        End Get
        Set(value As Decimal)
            weightvaluedec = value
        End Set
    End Property

    Public Property WeightUnit As String
        Get
            Return weightunitstring

        End Get
        Set(value As String)
            weightunitstring = value
        End Set
    End Property


    Public Property DimensionQuantity As Decimal
        Get
            Return dimensionquantitydec

        End Get
        Set(value As Decimal)
            dimensionquantitydec = value
        End Set
    End Property


    Public Property DimensionLength As Decimal
        Get
            Return dimensionlengthdec

        End Get
        Set(value As Decimal)
            dimensionlengthdec = value
        End Set
    End Property


    Public Property DimensionWidth As Decimal
        Get
            Return dimensionwidthdec

        End Get
        Set(value As Decimal)
            dimensionwidthdec = value
        End Set
    End Property


    Public Property DimensionHeight As Decimal
        Get
            Return dimensionheightdec

        End Get
        Set(value As Decimal)
            dimensionheightdec = value
        End Set
    End Property

End Class
