﻿Imports System.ComponentModel

Public Class Customer

    Implements INotifyPropertyChanged

    Private customercode As String
    Private customername As String

    Public Property Customer As String
        Get
            Return customercode
        End Get
        Set(ByVal value As String)
            customercode = value
            ' Call OnPropertyChanged whenever the property is updated
            OnPropertyChanged("code")
        End Set
    End Property

    Public Property Name As String
        Get
            Return customername
        End Get
        Set(ByVal value As String)
            customername = value
            ' Call OnPropertyChanged whenever the property is updated
            OnPropertyChanged("name")
        End Set
    End Property


    ' Declare the event
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    ' Create the OnPropertyChanged method to raise the event
    Protected Sub OnPropertyChanged(ByVal name As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(name))
    End Sub


End Class
