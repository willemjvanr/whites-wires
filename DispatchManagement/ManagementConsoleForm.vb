﻿Imports System.ComponentModel
Imports System.Collections.Specialized
Imports System.Collections.ObjectModel
Imports System.IO
Imports System.Configuration

Public Class ManagementConsoleForm

    Private WithEvents logicobject As BusinessLogic
    Private Grouped As Integer = 0
    Private SelectAll As Boolean = False
    Sub New()
        Try

            ' This call is required by the designer.
            InitializeComponent()
            logicobject = New BusinessLogic()
            Dim cst = New Customer
            cst.Customer = ""
            cst.Name = ""
            logicobject.SelectedCustomer = cst
            ' Add any initialization after the InitializeComponent() call.
            CustomerTextBox.DataBindings.Add(New Binding("Text", logicobject.SelectedCustomer, "Customer"))

            With DispatchNoteListView
                .View = View.Details
                .Columns.Add("Items")
                .Columns.Add("Item Description")
            End With

            UnProcessedConsignmentDataGridView.DataSource = logicobject.FinalConsignments
            UnProcessedConsignmentDataGridView.AllowUserToOrderColumns = True
            SetColumnSortMode(UnProcessedConsignmentDataGridView, DataGridViewColumnSortMode.Automatic)

            For Each clm As DataGridViewColumn In UnProcessedConsignmentDataGridView.Columns
                If clm.HeaderText = "PickupName" Or clm.HeaderText = "PickupAddress1" Or clm.HeaderText = "PickupAddress2" Or clm.HeaderText = "PickupSuburb" Or clm.HeaderText = "PickupState" _
                    Or clm.HeaderText = "PickupPostCode" Or clm.HeaderText = "PickupPhone" Or clm.HeaderText = "PickupCode" Or clm.HeaderText = "SenderCode" Or clm.HeaderText = "SenderName" _
                    Or clm.HeaderText = "SenderAddress1" Or clm.HeaderText = "SenderAddress2" Or clm.HeaderText = "SenderSuburb" Or clm.HeaderText = "SenderState" _
                    Or clm.HeaderText = "SenderPostCode" Or clm.HeaderText = "SenderPhone" Or clm.HeaderText = "SenderCity" Or clm.HeaderText = "PickupCity" Or clm.HeaderText = "ChargeCode" _
                    Or clm.HeaderText = "ReceiverAddress1" Or clm.HeaderText = "ReceiverAddress2" Or clm.HeaderText = "ReceiverPostCode" Or clm.HeaderText = "Contract" Or clm.HeaderText = "ServiceRequired" _
                    Or clm.HeaderText = "Processed" Or clm.HeaderText = "Printed" Or clm.HeaderText = "Complete" Or clm.HeaderText = "DispatchDate" Or clm.HeaderText = "BatchID" Then
                    clm.Visible = False
                End If
                If clm.HeaderText = "SenderReference" Then
                    clm.HeaderText = "Dispatch Note"
                End If

            Next
        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Public Shared Sub SetColumnSortMode(dataGridView As DataGridView, sortMode As DataGridViewColumnSortMode)
        For Each column As DataGridViewColumn In dataGridView.Columns
            column.SortMode = sortMode
        Next
    End Sub

    ''' <summary>
    ''' Listener event. If properties change in the logic object the form needs to react. this event listens to the customer and warehouse properties for change.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Logic_PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Handles logicobject.PropertyChanged
        Try

            If e.PropertyName = "selectcustomer" Then
                If ConsignmentTabControl.SelectedTab.Name.ToString = "CreateConsigmentTab" Then
                    CustomerTextBox.Text = logicobject.SelectedCustomer.Customer
                    ''WarehouseTextBox.Text = ""
                    logicobject.BuildNewDispatchNotes(CustomerTextBox.Text, WarehouseTextBox.Text, ShippingInstructionTextBox.Text)

                
                Else

                    ''UnProcessedConsignmentWarehouseTextBox.Text = ""
                    UnProcessedConsignmentDataGridView.DataSource = logicobject.PopulateAllUnProcessedConsignments(SelectAll, UnProcessedConsignmentWarehouseTextBox.Text, ShippingInstructFinaliseTextBox.Text, Grouped)

                End If
                If logicobject.DefaultWarehouse <> "" Then
                    Dim SelectWH As New Warehouse
                    SelectWH.Warehouse = logicobject.DefaultWarehouse
                    logicobject.SelectedWarehouse = SelectWH
                End If
            ElseIf e.PropertyName = "selectwarehouse" Then



                WarehouseTextBox.Text = logicobject.SelectedWarehouse.Warehouse
                UnProcessedConsignmentWarehouseTextBox.Text = logicobject.SelectedWarehouse.Warehouse

                If ConsignmentTabControl.SelectedTab.Name.ToString = "CreateConsigmentTab" Then
                    logicobject.BuildNewDispatchNotes(CustomerTextBox.Text, WarehouseTextBox.Text, ShippingInstructionTextBox.Text)
                
                Else

                    UnProcessedConsignmentDataGridView.DataSource = logicobject.PopulateAllUnProcessedConsignments(SelectAll, UnProcessedConsignmentWarehouseTextBox.Text, UnProcessedConsignmentWarehouseTextBox.Text, Grouped)

                End If
                My.Settings.DefaultWarehouse = logicobject.SelectedWarehouse.Warehouse
                My.Settings.Save()

            ElseIf e.PropertyName = "selectinstruction" Then
                If ConsignmentTabControl.SelectedTab.Name.ToString = "CreateConsigmentTab" Then
                    ShippingInstructionTextBox.Text = logicobject.SelectedShippingInstruction.ShippingInstruction
                    logicobject.BuildNewDispatchNotes(CustomerTextBox.Text, WarehouseTextBox.Text, ShippingInstructionTextBox.Text)
               
                Else
                    ShippingInstructFinaliseTextBox.Text = logicobject.SelectedShippingInstruction.ShippingInstruction
                    UnProcessedConsignmentWarehouseTextBox.Text = logicobject.SelectedWarehouse.Warehouse
                    UnProcessedConsignmentDataGridView.DataSource = logicobject.PopulateAllUnProcessedConsignments(SelectAll, UnProcessedConsignmentWarehouseTextBox.Text, ShippingInstructFinaliseTextBox.Text, Grouped)

                End If



            ElseIf e.PropertyName = "linkadd" Then
                DispatchNoteTextBox.Text = logicobject.SelectedDispatchNote.DispatchNote
                CustomerTextBox.Text = logicobject.SelectedDispatchNote.SYSPROSalesOrder
                POTextBox.Text = logicobject.GetPO(logicobject.SelectedDispatchNote.DispatchNote)
                CustomerTextBox.Text = logicobject.GetCustomer(logicobject.SelectedDispatchNote.DispatchNote)
                NoOfLinesTextBox.Text = logicobject.GetDispatchLines(logicobject.SelectedDispatchNote.DispatchNote)
                KGTextBox.Text = "0"
            End If
        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try
    End Sub


    ''' <summary>
    ''' This is another listener event and if a collection changes in the logic object then the screen needs to react.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Logic_CollectionChanged(sender As Object, e As NotifyCollectionChangedEventArgs) Handles logicobject.CollectionChanged
        Try
            If e.Action = NotifyCollectionChangedAction.Reset Then

                'LinkDispatchNotesDataGridView.DataSource = Nothing
                'LinkDispatchNotesDataGridView.DataSource = logicobject.LinkDispatchNotes
                'NewDispatchNotesDataGridView.DataSource = Nothing
                'NewDispatchNotesDataGridView.DataSource = logicobject.NewDispatchNotes

            End If

        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try
    End Sub

#Region "Create Consignment Events"
    ''' <summary>
    ''' This event opens the customer lookup screen
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub SearchCustomerButton_Click(sender As Object, e As EventArgs)
        Try

            Dim SearchWindow As New CustomerListForm(logicobject)

            SearchWindow.ShowDialog()
        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try


    End Sub




    Private Sub WarehouseSearchButton_Click(sender As Object, e As EventArgs) Handles WarehouseSearchButton.Click
        Try

            Dim SearchWindow As New WarehouseListForm(logicobject)

            SearchWindow.ShowDialog()
        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ShippingInstructionButton_Click(sender As Object, e As EventArgs) Handles ShippingInstructionButton.Click
        Try
            Dim SearchWindow As New ShippingInstructionListForm(logicobject)

            SearchWindow.ShowDialog()
        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub ConsignmentCustomerSearchButton_Click(sender As Object, e As EventArgs)
        Try

            Dim SearchWindow As New CustomerListForm(logicobject)

            SearchWindow.ShowDialog()
        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub DispatchSaveButton_Click(sender As Object, e As EventArgs) Handles DispatchSaveButton.Click
        Try

            If ValidateControl() Then
                SearchDispatchButton.Enabled = False
                Dim NewDispatchItem As New DispatchNotes
                NewDispatchItem.Items = ItemsNumericUpDown.Value
                NewDispatchItem.Description = ItemDescriptionComboBox.Text
                NewDispatchItem.CubicValue = logicobject.SelectedConsignmentDetail.CubicValue
                NewDispatchItem.CubicUnit = CubicUnitTextBox.Text
                NewDispatchItem.WeightValue = logicobject.SelectedConsignmentDetail.WeightValue
                NewDispatchItem.WeightUnit = WeightUnitTextBox.Text
                NewDispatchItem.DimensionQuantity = DimensionQuantityNumericUpDown.Value
                NewDispatchItem.DimensionLength = DimensionLengthNumericUpDown.Value
                NewDispatchItem.DimensionWidth = DimensionWidthNumericUpDown.Value
                NewDispatchItem.DimensionHeight = DimensionHeightNumericUpDown.Value


                NewDispatchItem.DispatchNote = logicobject.SelectedDispatchNote.DispatchNote
                NewDispatchItem.ConsignmentNumber = logicobject.SelectedDispatchNote.ConsignmentNumber
                NewDispatchItem.SYSPROSalesOrder = logicobject.SelectedDispatchNote.SYSPROSalesOrder

                logicobject.LinkDispatchNotes.Add(NewDispatchItem)
                DispatchNoteListView.Items.Clear()

                KGTextBox.Text = "0"

                For Each disp As DispatchNotes In logicobject.LinkDispatchNotes

                    If disp.DispatchNote = logicobject.SelectedDispatchNote.DispatchNote Then
                        Dim listitm As New ListViewItem(New String() {disp.Items, disp.Description})


                        KGTextBox.Text = (Convert.ToInt32(KGTextBox.Text) + disp.WeightValue).ToString

                        DispatchNoteListView.Items.Add(listitm)



                    End If


                Next
                ItemsNumericUpDown.Value = 0
                ItemDescriptionComboBox.Text = ""
                CubicUnitTextBox.Text = ""
                WeightUnitTextBox.Text = ""
                DimensionQuantityNumericUpDown.Value = 0
                DimensionLengthNumericUpDown.Value = 0
                DimensionWidthNumericUpDown.Value = 0
                DimensionHeightNumericUpDown.Value = 0


            Else
                MessageBox.Show("All the fields are required to have values")
            End If


        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try


    End Sub

    Private Function ValidateControl() As Boolean

        Dim Valid = False

        If ItemsNumericUpDown.Value = 0 Then
            Valid = False
            Exit Function
        Else
            Valid = True
        End If

        If ItemDescriptionComboBox.Text = "" Then
            Valid = False
            Exit Function
        Else
            Valid = True
        End If

        


        If CubicUnitTextBox.Text = "" Then
            Valid = False
            Exit Function
        Else
            Valid = True
        End If



        If WeightUnitTextBox.Text = "" Then
            Valid = False
            Exit Function
        Else
            Valid = True
        End If

        If DimensionQuantityNumericUpDown.Value = 0 Then
            Valid = False
            Exit Function
        Else
            Valid = True
        End If

        If DimensionLengthNumericUpDown.Value = 0 Then
            Valid = False
            Exit Function
        Else
            Valid = True
        End If

        If DimensionWidthNumericUpDown.Value = 0 Then
            Valid = False
            Exit Function
        Else
            Valid = True
        End If

        If DimensionHeightNumericUpDown.Value = 0 Then
            Valid = False
            Exit Function
        Else
            Valid = True
        End If



        Return Valid
    End Function

    Private Sub DispatchCancelButton_Click(sender As Object, e As EventArgs) Handles DispatchCancelButton.Click

        CompleteButton.Enabled = True
        FinishButton.Enabled = False
        PrintLabelsButton.Enabled = False
        DispatchSaveButton.Enabled = True
        SearchDispatchButton.Enabled = True
        DispatchNoteListView.Clear()
        With DispatchNoteListView
            .View = View.Details
            .Columns.Add("Items")
            .Columns.Add("Item Description")
        End With
        logicobject.LinkDispatchNotes = New ObservableCollection(Of DispatchNotes)

    End Sub

    Private Sub CompleteButton_Click(sender As Object, e As EventArgs) Handles CompleteButton.Click
        logicobject.CreateConsignment()
        logicobject.ResetDispatchList(logicobject.SelectedDispatchNote)
        logicobject.LinkDispatchNotes = logicobject.LinkDispatchNotes
        CompleteButton.Enabled = False
        PrintLabelsButton.Enabled = True
        FinishButton.Enabled = True

        DispatchSaveButton.Enabled = False
        'Me.Close()
    End Sub

    Private Sub SearchDispatchButton_Click(sender As Object, e As EventArgs) Handles SearchDispatchButton.Click
        Try
            logicobject.SelectedDispatchNote = New DispatchNotes

            Dim SearchWindow As New DispatchNoteListForm(logicobject)

            SearchWindow.ShowDialog()
        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try
    End Sub




#End Region

#Region "Merge Consignment Events"
    Private Sub ConsignmentWarehouseSearchButton_Click(sender As Object, e As EventArgs)
        Try

            Dim SearchWindow As New WarehouseListForm(logicobject)

            SearchWindow.ShowDialog()
        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try
    End Sub

   

#End Region

  

   

    Private Sub CustomerTextBox_Leave(sender As Object, e As EventArgs)
        Dim CustomerSelect As New Customer

        CustomerSelect.Customer = CustomerTextBox.Text
        CustomerSelect.Name = ""



        logicobject.SelectedCustomer = CustomerSelect
    End Sub

    Private Sub UnProcessedConsignmentCustomerButton_Click(sender As Object, e As EventArgs)
        Try

            Dim SearchWindow As New CustomerListForm(logicobject)

            SearchWindow.ShowDialog()
        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub UnProcessedConsignmentWarehouseButton_Click(sender As Object, e As EventArgs) Handles UnProcessedConsignmentWarehouseButton.Click
        Try

            Dim SearchWindow As New WarehouseListForm(logicobject)

            SearchWindow.ShowDialog()
        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ProcessButton_Click(sender As Object, e As EventArgs) Handles ProcessButton.Click

        Dim Filedlg As New SaveFileDialog()

        Filedlg.FileName = "ShippingDoc_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml"

        Filedlg.ShowDialog()
        Dim Consignments As String = ""

        If Filedlg.FileName <> "" Then
            For Each row As DataGridViewRow In UnProcessedConsignmentDataGridView.Rows
                If row.Cells(0).Value Then
                    Consignments += row.Cells(1).Value.ToString + ","

                End If
            Next
            Dim Xml As String

            Xml = logicobject.GetConsignmentFinalXML(Consignments)

            Using sw As New StreamWriter(Filedlg.FileName)

                sw.Write(Xml)

            End Using
            UnProcessedConsignmentDataGridView.DataSource = logicobject.PopulateAllUnProcessedConsignments(SelectAll, UnProcessedConsignmentWarehouseTextBox.Text, ShippingInstructFinaliseTextBox.Text, Grouped)

        End If





    End Sub

    Private Sub ConsignmentTabControl_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ConsignmentTabControl.SelectedIndexChanged
        If ConsignmentTabControl.SelectedTab.Name.ToString = "CreateConsigmentTab" Then

            logicobject.BuildNewDispatchNotes("", WarehouseTextBox.Text, ShippingInstructionTextBox.Text)
            'NewDispatchNotesDataGridView.AutoGenerateColumns = False
            'NewDispatchNotesDataGridView.DataSource = logicobject.NewDispatchNotes
       
        Else
            TotalCubicLabel.Text = "TOTAL CUBIC for Manifest = 0"
            TotalWeightLabel.Text = "TOTAL WEIGHT for Manifest = 0"
            UnProcessedConsignmentDataGridView.DataSource = logicobject.PopulateAllUnProcessedConsignments(SelectAll, UnProcessedConsignmentWarehouseTextBox.Text, ShippingInstructFinaliseTextBox.Text, Grouped)

            For Each clm As DataGridViewColumn In UnProcessedConsignmentDataGridView.Columns
                If clm.HeaderText = "PickupName" Or clm.HeaderText = "PickupAddress1" Or clm.HeaderText = "PickupAddress2" Or clm.HeaderText = "PickupSuburb" Or clm.HeaderText = "PickupState" _
                    Or clm.HeaderText = "PickupPostCode" Or clm.HeaderText = "PickupPhone" Or clm.HeaderText = "PickupCode" Or clm.HeaderText = "SenderCode" Or clm.HeaderText = "SenderName" _
                    Or clm.HeaderText = "SenderAddress1" Or clm.HeaderText = "SenderAddress2" Or clm.HeaderText = "SenderSuburb" Or clm.HeaderText = "SenderState" _
                    Or clm.HeaderText = "SenderPostCode" Or clm.HeaderText = "SenderPhone" Or clm.HeaderText = "SenderCity" Or clm.HeaderText = "PickupCity" Or clm.HeaderText = "ChargeCode" _
                    Or clm.HeaderText = "ReceiverAddress1" Or clm.HeaderText = "ReceiverAddress2" Or clm.HeaderText = "ReceiverPostCode" Or clm.HeaderText = "Contract" Or clm.HeaderText = "ServiceRequired" _
                    Or clm.HeaderText = "Processed" Or clm.HeaderText = "Printed" Or clm.HeaderText = "Complete" Or clm.HeaderText = "DispatchDate" Or clm.HeaderText = "BatchID" Then
                    clm.Visible = False
                End If
                If clm.HeaderText = "SenderReference" Then
                    clm.HeaderText = "Dispatch Note"
                End If

            Next

        End If
    End Sub

    Private Sub ShipInstructFinaliseButton_Click(sender As Object, e As EventArgs) Handles ShipInstructFinaliseButton.Click
        Try
            Dim SearchWindow As New ShippingInstructionListForm(logicobject)

            SearchWindow.ShowDialog()
        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FinishButton_Click(sender As Object, e As EventArgs) Handles FinishButton.Click
        Dim BlankDispatchOUt As New DispatchNotes()
        BlankDispatchOUt.DispatchNote = ""
        BlankDispatchOUt.SYSPROSalesOrder = ""
        logicobject.SelectedDispatchNote = BlankDispatchOUt
        logicobject.LinkDispatchNotes = New ObservableCollection(Of DispatchNotes)
        DispatchNoteListView.Items.Clear()
        ItemsNumericUpDown.Value = 0
        ItemDescriptionComboBox.Text = ""
        CubicUnitTextBox.Text = ""
        WeightUnitTextBox.Text = ""
        DimensionQuantityNumericUpDown.Value = 0
        DimensionLengthNumericUpDown.Value = 0
        DimensionWidthNumericUpDown.Value = 0
        DimensionHeightNumericUpDown.Value = 0
        CompleteButton.Enabled = True
        FinishButton.Enabled = False
        PrintLabelsButton.Enabled = False
        DispatchSaveButton.Enabled = True
        SearchDispatchButton.Enabled = True
    End Sub

    Private Sub SelectAllCheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles SelectAllCheckBox.CheckedChanged
        SelectAll = SelectAllCheckBox.Checked
        UnProcessedConsignmentDataGridView.DataSource = logicobject.PopulateAllUnProcessedConsignments(SelectAll, UnProcessedConsignmentWarehouseTextBox.Text, ShippingInstructFinaliseTextBox.Text, Grouped)


        For Each clm As DataGridViewColumn In UnProcessedConsignmentDataGridView.Columns
            If clm.HeaderText = "PickupName" Or clm.HeaderText = "PickupAddress1" Or clm.HeaderText = "PickupAddress2" Or clm.HeaderText = "PickupSuburb" Or clm.HeaderText = "PickupState" _
                Or clm.HeaderText = "PickupPostCode" Or clm.HeaderText = "PickupPhone" Or clm.HeaderText = "PickupCode" Or clm.HeaderText = "SenderCode" Or clm.HeaderText = "SenderName" _
                Or clm.HeaderText = "SenderAddress1" Or clm.HeaderText = "SenderAddress2" Or clm.HeaderText = "SenderSuburb" Or clm.HeaderText = "SenderState" _
                Or clm.HeaderText = "SenderPostCode" Or clm.HeaderText = "SenderPhone" Or clm.HeaderText = "SenderCity" Or clm.HeaderText = "PickupCity" Or clm.HeaderText = "ChargeCode" _
                Or clm.HeaderText = "ReceiverAddress1" Or clm.HeaderText = "ReceiverAddress2" Or clm.HeaderText = "ReceiverPostCode" Or clm.HeaderText = "Contract" Or clm.HeaderText = "ServiceRequired" _
                Or clm.HeaderText = "Processed" Or clm.HeaderText = "Printed" Or clm.HeaderText = "Complete" Or clm.HeaderText = "DispatchDate" Or clm.HeaderText = "BatchID" Then
                clm.Visible = False
            End If
            If clm.HeaderText = "SenderReference" Then
                clm.HeaderText = "Dispatch Note"
            End If

        Next


        TotalCubicLabel.Text = "TOTAL CUBIC for Manifest = 0"
        TotalWeightLabel.Text = "TOTAL WEIGHT for Manifest = 0"
        KGTextBox.Text = "0"
        Dim cubic As Decimal = 0
        Dim Weight As Decimal = 0

        For Each disp As WW_MainfreightHeader_CustomSelect In logicobject.FinalConsignments

            If disp.Selected Then

                cubic += disp.TotalCubic
                Weight += disp.TotalWeight
            End If


        Next

        TotalCubicLabel.Text = "TOTAL CUBIC for Manifest = " + cubic.ToString
        TotalWeightLabel.Text = "TOTAL WEIGHT for Manifest = " + Weight.ToString


    End Sub


    Private order As Boolean = False
    Private Sub UnProcessedConsignmentDataGridView_ColumnHeaderMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles UnProcessedConsignmentDataGridView.ColumnHeaderMouseClick
        logicobject.PopulateAllUnProcessedConsignments(SelectAll, UnProcessedConsignmentWarehouseTextBox.Text, ShippingInstructFinaliseTextBox.Text, Grouped)

        

        If order Then
            Dim sortedlist = logicobject.FinalConsignments.OrderByDescending(Function(o) o.SysproCustomer)
            logicobject.FinalConsignments = New ObservableCollection(Of WW_MainfreightHeader_CustomSelect)
            For Each item As WW_MainfreightHeader_CustomSelect In sortedlist
                logicobject.FinalConsignments.Add(item)
            Next
            order = False
        Else
            Dim sortedlist = logicobject.FinalConsignments.OrderBy(Function(o) o.SysproCustomer)
            logicobject.FinalConsignments = New ObservableCollection(Of WW_MainfreightHeader_CustomSelect)
            For Each item As WW_MainfreightHeader_CustomSelect In sortedlist
                logicobject.FinalConsignments.Add(item)
            Next
            order = True
        End If
        UnProcessedConsignmentDataGridView.DataSource = logicobject.FinalConsignments

        For Each clm As DataGridViewColumn In UnProcessedConsignmentDataGridView.Columns
            If clm.HeaderText = "PickupName" Or clm.HeaderText = "PickupAddress1" Or clm.HeaderText = "PickupAddress2" Or clm.HeaderText = "PickupSuburb" Or clm.HeaderText = "PickupState" _
                Or clm.HeaderText = "PickupPostCode" Or clm.HeaderText = "PickupPhone" Or clm.HeaderText = "PickupCode" Or clm.HeaderText = "SenderCode" Or clm.HeaderText = "SenderName" _
                Or clm.HeaderText = "SenderAddress1" Or clm.HeaderText = "SenderAddress2" Or clm.HeaderText = "SenderSuburb" Or clm.HeaderText = "SenderState" _
                Or clm.HeaderText = "SenderPostCode" Or clm.HeaderText = "SenderPhone" Or clm.HeaderText = "SenderCity" Or clm.HeaderText = "PickupCity" Or clm.HeaderText = "ChargeCode" _
                Or clm.HeaderText = "ReceiverAddress1" Or clm.HeaderText = "ReceiverAddress2" Or clm.HeaderText = "ReceiverPostCode" Or clm.HeaderText = "Contract" Or clm.HeaderText = "ServiceRequired" _
                Or clm.HeaderText = "Processed" Or clm.HeaderText = "Printed" Or clm.HeaderText = "Complete" Or clm.HeaderText = "DispatchDate" Or clm.HeaderText = "BatchID" Then
                clm.Visible = False
            End If
            If clm.HeaderText = "SenderReference" Then
                clm.HeaderText = "Dispatch Note"
            End If

        Next
    End Sub

    Private Sub UnProcessedConsignmentDataGridView_DataBindingComplete(sender As Object, e As DataGridViewBindingCompleteEventArgs) Handles UnProcessedConsignmentDataGridView.DataBindingComplete

        Dim customer = ""
        For Each row As DataGridViewRow In UnProcessedConsignmentDataGridView.Rows
            If row.Cells(4).Value = customer Then
                row.DefaultCellStyle.ForeColor = Color.Red
            Else
                row.DefaultCellStyle.ForeColor = Color.Black
            End If

            customer = row.Cells(4).Value
        Next
    End Sub

    Private Sub MergeConsigmentButton_Click(sender As Object, e As EventArgs) Handles MergeConsigmentButton.Click
        Dim list As New ObservableCollection(Of WW_MainfreightHeader_CustomSelect)

        list = UnProcessedConsignmentDataGridView.DataSource
        logicobject.FinalConsignments = list
        If logicobject.CheckCustomer Then
            logicobject.MergeConsignments()
            UnProcessedConsignmentDataGridView.DataSource = logicobject.PopulateAllUnProcessedConsignments(SelectAll, UnProcessedConsignmentWarehouseTextBox.Text, ShippingInstructFinaliseTextBox.Text, Grouped)

            For Each clm As DataGridViewColumn In UnProcessedConsignmentDataGridView.Columns
                If clm.HeaderText = "PickupName" Or clm.HeaderText = "PickupAddress1" Or clm.HeaderText = "PickupAddress2" Or clm.HeaderText = "PickupSuburb" Or clm.HeaderText = "PickupState" _
                    Or clm.HeaderText = "PickupPostCode" Or clm.HeaderText = "PickupPhone" Or clm.HeaderText = "PickupCode" Or clm.HeaderText = "SenderCode" Or clm.HeaderText = "SenderName" _
                    Or clm.HeaderText = "SenderAddress1" Or clm.HeaderText = "SenderAddress2" Or clm.HeaderText = "SenderSuburb" Or clm.HeaderText = "SenderState" _
                    Or clm.HeaderText = "SenderPostCode" Or clm.HeaderText = "SenderPhone" Or clm.HeaderText = "SenderCity" Or clm.HeaderText = "PickupCity" Or clm.HeaderText = "ChargeCode" _
                    Or clm.HeaderText = "ReceiverAddress1" Or clm.HeaderText = "ReceiverAddress2" Or clm.HeaderText = "ReceiverPostCode" Or clm.HeaderText = "Contract" Or clm.HeaderText = "ServiceRequired" _
                    Or clm.HeaderText = "Processed" Or clm.HeaderText = "Printed" Or clm.HeaderText = "Complete" Or clm.HeaderText = "DispatchDate" Or clm.HeaderText = "BatchID" Then
                    clm.Visible = False
                End If
                If clm.HeaderText = "SenderReference" Then
                    clm.HeaderText = "Dispatch Note"
                End If

            Next
        Else
            MessageBox.Show("Cannot merge consigments for different customers!")
        End If
    End Sub

    Private Sub GroupingCheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles GroupingCheckBox.CheckedChanged
        If GroupingCheckBox.Checked Then
            Grouped = 1
        Else
            Grouped = 0
        End If
        UnProcessedConsignmentDataGridView.DataSource = logicobject.PopulateAllUnProcessedConsignments(SelectAll, UnProcessedConsignmentWarehouseTextBox.Text, ShippingInstructFinaliseTextBox.Text, Grouped)

        For Each clm As DataGridViewColumn In UnProcessedConsignmentDataGridView.Columns
            If clm.HeaderText = "PickupName" Or clm.HeaderText = "PickupAddress1" Or clm.HeaderText = "PickupAddress2" Or clm.HeaderText = "PickupSuburb" Or clm.HeaderText = "PickupState" _
                Or clm.HeaderText = "PickupPostCode" Or clm.HeaderText = "PickupPhone" Or clm.HeaderText = "PickupCode" Or clm.HeaderText = "SenderCode" Or clm.HeaderText = "SenderName" _
                Or clm.HeaderText = "SenderAddress1" Or clm.HeaderText = "SenderAddress2" Or clm.HeaderText = "SenderSuburb" Or clm.HeaderText = "SenderState" _
                Or clm.HeaderText = "SenderPostCode" Or clm.HeaderText = "SenderPhone" Or clm.HeaderText = "SenderCity" Or clm.HeaderText = "PickupCity" Or clm.HeaderText = "ChargeCode" _
                Or clm.HeaderText = "ReceiverAddress1" Or clm.HeaderText = "ReceiverAddress2" Or clm.HeaderText = "ReceiverPostCode" Or clm.HeaderText = "Contract" Or clm.HeaderText = "ServiceRequired" _
                Or clm.HeaderText = "Processed" Or clm.HeaderText = "Printed" Or clm.HeaderText = "Complete" Or clm.HeaderText = "DispatchDate" Or clm.HeaderText = "BatchID" Then
                clm.Visible = False
            End If
            If clm.HeaderText = "SenderReference" Then
                clm.HeaderText = "Dispatch Note"
            End If

        Next
    End Sub

    Private Sub UnProcessedConsignmentDataGridView_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles UnProcessedConsignmentDataGridView.CellValueChanged
        Dim cubic As Decimal = 0
        Dim Weight As Decimal = 0
        Dim list As New ObservableCollection(Of WW_MainfreightHeader_CustomSelect)

        list = UnProcessedConsignmentDataGridView.DataSource
        For Each disp As WW_MainfreightHeader_CustomSelect In list

            If disp.Selected Then

                cubic += disp.TotalCubic
                Weight += disp.TotalWeight
            End If


        Next

        TotalCubicLabel.Text = "TOTAL CUBIC for Manifest = " + cubic.ToString
        TotalWeightLabel.Text = "TOTAL WEIGHT for Manifest = " + Weight.ToString
    End Sub

    Private Sub EditButton_Click(sender As Object, e As EventArgs) Handles EditButton.Click
        Dim list As New ObservableCollection(Of WW_MainfreightHeader_CustomSelect)

        list = UnProcessedConsignmentDataGridView.DataSource
        logicobject.FinalConsignments = list
        For Each item As WW_MainfreightHeader_CustomSelect In logicobject.FinalConsignments
            If item.Selected Then
               

                Dim DispatchSelect As New WW_MainfreightHeader_Custom

                DispatchSelect.ConsignmentNo = item.ConsignmentNo
                DispatchSelect.ConsignmentDate = item.ConsignmentDate
                DispatchSelect.TotalCubic = item.TotalCubic
                DispatchSelect.TotalWeight = item.TotalWeight
                DispatchSelect.SenderReference = item.SenderReference
                DispatchSelect.SysproCustomer = item.SysproCustomer


                logicobject.SelectedConsignment = DispatchSelect
            End If
        Next

        Dim ConsignEdit As New AddDispatchNoteForm(logicobject)

        ConsignEdit.ShowDialog()




    End Sub

    Private Sub SettingsButton_Click(sender As Object, e As EventArgs) Handles SettingsButton.Click
        Dim setting As New Settings
        setting.ShowDialog()
    End Sub

    Private Sub PrintLabelsButton_Click(sender As Object, e As EventArgs) Handles PrintLabelsButton.Click
        Dim DispatchNote As String = DispatchNoteTextBox.Text
        Dim command As String = String.Format(My.Settings.LabelAutomation, DispatchNote)

        Interaction.Shell(Command, AppWinStyle.MinimizedNoFocus, True, 60000)

           
    End Sub

    Private Sub RePrintButton_Click(sender As Object, e As EventArgs) Handles RePrintButton.Click

        Dim list As New ObservableCollection(Of WW_MainfreightHeader_CustomSelect)

        list = UnProcessedConsignmentDataGridView.DataSource
        logicobject.FinalConsignments = list
        For Each item As WW_MainfreightHeader_CustomSelect In logicobject.FinalConsignments
            If item.Selected Then

                Dim DispatchNote As String = item.SenderReference
                Dim command As String = String.Format(My.Settings.LabelAutomation, DispatchNote)

                Interaction.Shell(command, AppWinStyle.MinimizedNoFocus, True, 60000)
            End If
        Next


    End Sub
End Class
