

/****** Object:  Table [dbo].[WW_MainfreightHeader_Custom]    Script Date: 23/11/2016 16:15:41 ******/
DROP TABLE [dbo].[WW_MainfreightHeader_Custom]
GO

/****** Object:  Table [dbo].[WW_MainfreightHeader_Custom]    Script Date: 23/11/2016 16:15:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[WW_MainfreightHeader_Custom](
	[ConsignmentNo] [varchar](50) NOT NULL,
	[ConsignmentDate] [datetime] NULL,
	[SysproWarehouse] [varchar](20) NOT NULL,
	[SysproCustomer] [varchar](20) NOT NULL,
	[SysproOperator] [varchar](50) NOT NULL,
	[PickupCode] [varchar](50) NOT NULL,
	[PickupName] [varchar](50) NOT NULL,
	[PickupAddress1] [varchar](50) NOT NULL,
	[PickupAddress2] [varchar](50) NULL,
	[PickupSuburb] [varchar](50) NOT NULL,
	[PickupCity] [varchar](50) NOT NULL,
	[PickupState] [varchar](4) NOT NULL,
	[PickupPostCode] [varchar](10) NOT NULL,
	[PickupPhone] [varchar](50) NULL,
	[ChargeCode] [varchar](50) NOT NULL,
	[SenderCode] [varchar](50) NOT NULL,
	[SenderName] [varchar](50) NOT NULL,
	[SenderAddress1] [varchar](50) NOT NULL,
	[SenderAddress2] [varchar](50) NULL,
	[SenderSuburb] [varchar](50) NOT NULL,
	[SenderCity] [varchar](50) NOT NULL,
	[SenderState] [varchar](4) NOT NULL,
	[SenderPostCode] [varchar](10) NOT NULL,
	[SenderPhone] [varchar](50) NULL,
	[SenderReference] [varchar](50) NULL,
	[ReceiverName] [varchar](50) NOT NULL,
	[ReceiverAddress1] [varchar](50) NOT NULL,
	[ReceiverAddress2] [varchar](50) NULL,
	[ReceiverSuburb] [varchar](50) NOT NULL,
	[ReceiverCity] [varchar](50) NOT NULL,
	[ReceiverState] [varchar](4) NOT NULL,
	[ReceiverPostCode] [varchar](10) NOT NULL,
	[ReceiverPhone] [varchar](50) NULL,
	[ReceiverReference] [varchar](50) NULL,
	[ServiceRequired] [varchar](50) NULL,
	[Contract] [varchar](50) NULL,
	[TotalItems] [varchar](50) NULL,
	[TotalCubic] [varchar](50) NULL,
	[TotalWeight] [varchar](50) NULL,
	[Grouped] [bit] NOT NULL CONSTRAINT [DF_WW_MainfreightHeader_Custom_Grouped]  DEFAULT ((0)),
	[Processed] [bit] NOT NULL CONSTRAINT [DF_WW_MainfreightHeader_Custom_Processed]  DEFAULT ((0)),
	[Printed] [bit] NOT NULL CONSTRAINT [DF_WW_MainfreightHeader_Custom_Printed]  DEFAULT ((0)),
	[Complete] [bit] NOT NULL CONSTRAINT [DF_WW_MainfreightHeader_Custom_Complete]  DEFAULT ((0)),
	[BatchID] [bigint] NULL,
 CONSTRAINT [PK_WW_MainfreightHeader_Custom] PRIMARY KEY CLUSTERED 
(
	[ConsignmentNo] ASC,
	[SysproWarehouse] ASC,
	[SysproCustomer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [DF_WW__MainfreightHeader_Custom_ConsignmentNo] UNIQUE NONCLUSTERED 
(
	[ConsignmentNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


