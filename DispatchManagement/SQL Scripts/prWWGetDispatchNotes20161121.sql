
/****** Object:  StoredProcedure [dbo].[prWWGetDispatchNotes]    Script Date: 21/11/2016 16:47:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Procedure [dbo].[prWWGetDispatchNotes]
--Declare
@Customer nvarchar(30)
,@Warehouse nvarchar(10)
,@ShipInstr nvarchar(50)

As
--Select @Customer = '',@Warehouse = 'MS',@ShipInstr = ''

Select KeyDispatchNote as DispatchNote,'' as Consignment,SalesOrder as SYSPROSalesOrder 
from MdnMasterRep r with (NOLOCK) 
Left Join WW_MainfreightDetail_Custom w with (NOLOCK)
 on r.KeyDispatchNote = w.SysproDispatchNote and r.SalesOrder = w.SysproSalesOrder 
 Join (Select distinct DispatchNote,MWarehouse From MdnDetail Where MWarehouse <> '' and MWarehouse <> '**') d on d.DispatchNote = r.KeyDispatchNote
 Where Customer <> '' 
	and Customer like '%' +  @Customer + '%'
	and w.SysproDispatchNote is null
	and MWarehouse = @Warehouse
	and r.ShippingInstrsCod like '%' + @ShipInstr + '%'