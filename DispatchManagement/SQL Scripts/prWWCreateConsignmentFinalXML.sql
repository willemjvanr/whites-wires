
/****** Object:  StoredProcedure [dbo].[prWWCreateConsignmentFinalXML]    Script Date: 23/11/2016 14:13:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Willem Jansen
-- Create date: 2016/11/23
-- Version: 1.0
-- Description:	This script generates xml for the Manifest and updates the consignment table with a new batch id and set the consignments to processed
-- =============================================
Create Procedure [dbo].[prWWCreateConsignmentFinalXML]
--Declare 
@Consignment nvarchar(max)

--Select @Consignment = 'WWM002560001,WWM001390001,'
As

Select * 
Into #Consignments
from dbo.[WW_SplitString](@Consignment,',')


Declare @Warehouse nvarchar(30),@BatchID bigint

Select Top 1 @Warehouse = SysproWarehouse
From WW_MainfreightHeader_Custom cm
JOin #Consignments cc on cc.Item = cm.ConsignmentNo

Select top 1 @BatchID = BatchID From WW_MainfreightControl_Custom

Update WW_MainfreightControl_Custom
Set BatchID = BatchID + 1

Update WW_MainfreightHeader_Custom
Set BatchID = @BatchID,Processed = 1
From WW_MainfreightHeader_Custom cm
JOin #Consignments cc on cc.Item = cm.ConsignmentNo

Select Convert(varchar(max),(Select 
(Select 
	'356223' as SenderID
	,'MAINFEIGHT' as RecipientID
	,GetDate() as Prepared
	,Convert(numeric(18),GetDate()) as MessageID
	,'MANIFEST' as MessageType
	,'01' as MessageVersion
For Xml Path('MessageHeader'),type)
,(
Select 
(Select 
	'PICKUP' as 'Party/@Role'
	,Fax as 'Party/Code'
	,Description as 'Party/Name'
	,DeliveryAddr1 as 'Party/Address1'
	,DeliveryAddr2  as 'Party/Suburb'
	,DeliveryAddr3Loc as 'Party/City'
	,PostalCode as 'Party/PostalCode'
	,DeliveryAddr4 as 'Party/State'
	,'' as 'Party/Phone'
From InvWhControl
Where Warehouse = @Warehouse
For Xml Path('ManifestHeader'),type
)
,(Select ConsignmentNo
	,ConsignmentDate 
	,(Select * From (	
		Select 		
	   'CHARGE' as '@Role'
	   ,Fax as Code
		,null  as 'Name'
		,null  as 'Address1'
		,null as 'Suburb'
		,null  as 'City'
		,null 'PostalCode'
		,null  as 'State'
		,null as 'Phone' 
		From InvWhControl
		Where Warehouse = cm.SysproWarehouse
		Union All
		Select 
	   'SENDER' as '@Role'
	   ,c.SenderCode as Code
		,c.SenderName  as 'Name'
		,c.SenderAddress1  as 'Address1'
		,c.SenderSuburb as 'Suburb'
		,c.SenderCity  as 'City'
		,c.SenderPostCode 'PostalCode'
		,c.SenderState  as 'State'
		,c.SenderPhone as 'Phone' 
		from WW_MainfreightHeader_Custom c
			Where c.ConsignmentNo = cm.ConsignmentNo
		union all
		Select 
		   'RECEIVER' as '@Role'
		   ,null as Code
			,c.ReceiverName  as 'Name'
			,c.ReceiverAddress1  as 'Address1'
			,c.ReceiverSuburb as 'Suburb'
			,c.ReceiverCity  as 'City'
			,c.ReceiverPostCode 'PostalCode'
			,c.ReceiverState  as 'State'
			,c.ReceiverPhone as 'Phone' 
			from WW_MainfreightHeader_Custom c
			Where c.ConsignmentNo = cm.ConsignmentNo
			) as r
		For XML Path('Party'),type)
	,(Select * from (Select 'SENDER' as 'Reference/@Role'
							,c.SenderReference as Reference
						from WW_MainfreightHeader_Custom c
						Where c.ConsignmentNo = cm.ConsignmentNo
						Union All
						Select 'RECEIVER' as 'Reference/@Role'
							,c.ReceiverReference as Reference
						from WW_MainfreightHeader_Custom c
						Where c.ConsignmentNo = cm.ConsignmentNo
						)as r
		For XML Path(''),type)
	,'Express' as ServiceRequired
	,'' as Contact
	,(Select * from (Select 'ITEMS' as 'Total/@Units'
							,c.TotalItems as Total
						from WW_MainfreightHeader_Custom c
						Where c.ConsignmentNo = cm.ConsignmentNo
						Union All
						Select 'CUBIC' as 'Total/@Units'
							,c.TotalCubic as Total
						from WW_MainfreightHeader_Custom c
						Where c.ConsignmentNo = cm.ConsignmentNo
						Union All
						Select 'Weight' as 'Total/@Units'
							,c.TotalWeight as Total
						from WW_MainfreightHeader_Custom c
						Where c.ConsignmentNo = cm.ConsignmentNo
						)as r
		For XML Path(''),type)
	,(Select c.Items
							,c.Description 
							,(Select * From (Select 'CUBIC' as 'Measurement/@Property'
													,m.CubicValue as 'Measurement/Value'
													,m.CubicUnit as 'Measurement/Unit'
											from WW_MainfreightDetail_Custom m
											Where c.ConsignmentNo = m.ConsignmentNo and m.DetailID = c.DetailID
											Union All
											Select 'WEIGHT' as 'Measurement/@Property'
													,m.WeightValue as 'Measurement/Value'
													,m.WeightUnit as 'Measurement/Unit'
											from WW_MainfreightDetail_Custom m
											Where c.ConsignmentNo = m.ConsignmentNo and m.DetailID = c.DetailID) as r For XML Path(''),type)
								
						from WW_MainfreightDetail_Custom c
						Where c.ConsignmentNo = cm.ConsignmentNo
		For XML Path('Line'),type)
		
		
From WW_MainfreightHeader_Custom cm
Join InvWhControl wm on wm.Warehouse = cm.SysproWarehouse 
JOin #Consignments cc on cc.Item = cm.ConsignmentNo
Group BY ConsignmentNo
	,ConsignmentDate
	,SysproWarehouse
For XML Path('Consignment'),Root('Consignments'),type)
,'CONSIGNMENTS' as 'ControlTotal/Total/@Units'
,COUNT(*) as 'ControlTotal/Total'
From #Consignments 
For Xml Path('MessageBody'),type)
For Xml Path('Message')))



Drop TAble #Consignments

 