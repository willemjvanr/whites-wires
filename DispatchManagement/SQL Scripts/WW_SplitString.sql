
/****** Object:  UserDefinedFunction [dbo].[WW_SplitString]    Script Date: 23/11/2016 14:05:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-- =============================================
-- Author:		Willem Jansen
-- Create date: 2016/11/23
-- Version: 1.0
-- Description:	Splits a string delimited by a character and returns a table
-- =============================================
CREATE FUNCTION [dbo].[WW_SplitString](@String varchar(8000), @Delimiter char(1))    
   
returns @temptable TABLE (Item varchar(8000))       
as       
begin       
    declare @idx int       
    declare @slice varchar(8000)       
      
    select @idx = 1       
        if len(@String)<1 or @String is null  return       
      
    while @idx!= 0       
    begin       
        set @idx = charindex(@Delimiter,@String)       
        if @idx!=0       
            set @slice = left(@String,@idx - 1)       
        else       
            set @slice = @String       
          
        if(len(@slice)>0)  
            insert into @temptable(Item) values(@slice)       
  
        set @String = right(@String,len(@String) - @idx)       
        if len(@String) = 0 break       
    end   
return       
end