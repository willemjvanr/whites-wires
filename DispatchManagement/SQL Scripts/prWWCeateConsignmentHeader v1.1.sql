
/****** Object:  StoredProcedure [dbo].[prWWCeateConsignmentHeader]    Script Date: 21/11/2016 16:46:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Willem Jansen
-- Create date: 2016/11/08
-- Version: 1.1
-- Description:	This script generates xml for the Manifest
-- Change Date: 2016/11/23
-- Comments: Add BatchID
-- =============================================
ALTER Procedure [dbo].[prWWCeateConsignmentHeader]
--Declare 
@DispatchNote nvarchar(50),@Warehouse nvarchar(10)

As

--select @DispatchNote = 'M02624/0001',@Warehouse = 'BS'


Insert Into WW_MainfreightHeader_Custom
Select 'WW' + Replace(m.DispatchNote,'/','') as ConsignmentNo
		,GetDate() as ConsignmentDate
		,wc.Warehouse as SysproWarehouse
		,m.Customer as SysproCustomer
		,'' as SYSPROOperator
		,wc.Fax as PickupCode
		,wc.Description as PickupName
		,wc.DeliveryAddr1 as PickupAddress1
		,'' as PickupAddress2
		,wc.DeliveryAddr2 as PickupSuburb
		,wc.DeliveryAddr3Loc  as PickupCity
		,wc.DeliveryAddr4 as PickupState
		,wc.PostalCode as PickupPostCode
		,'' as PickupPhone
		,wc.Fax as ChargeCode
		,wc.Fax as SenderCode
		,wc.Description as SenderName
		,wc.DeliveryAddr1 as SenderAddress1
		,'' as SenderAddress2
		,wc.DeliveryAddr2 as SenderSuburb
		,wc.DeliveryAddr3Loc  as SenderCity
		,wc.DeliveryAddr4 as SenderState
		,wc.PostalCode as SenderPostCode
		,'' as SenderPhone
		,m.DispatchNote as SenderReference
		,m.DispatchCustName as ReceiverName
		,m.DispatchAddress1 as ReceiverAddress1
		,'' as ReceiverAddress2
		,m.DispatchAddress2 as ReceiverSuburb
		,m.DispatchAddrLoc as ReceiverCity
		,c.SoldToAddr4 as ReceiverState
		,m.DispatchPostalCode as ReceiverPostCode
		,'' as ReceiverPhone
		,'' as ReceiverReference
		,'' as ServiceRequired
		,'' as Contract
		,0 as TotalItems
		,0 as TotalCubic
		,0 as TotalWeight
		,0 as Grouped
		,0 as Processed
		,0 as Printed
		,0 as Complete
		,0 as BatchID
from MdnMaster m with (NOLOCK)
Join ArCustomer c with (NOLOCK) on c.Customer = m.Customer
left Join InvWhControl wc with (NOLOCK) on wc.Warehouse = @Warehouse
Where m.DispatchNote = @DispatchNote


Select 'WW' + Replace(@DispatchNote,'/','')