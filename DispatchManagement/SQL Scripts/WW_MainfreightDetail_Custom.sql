USE [SysproCompanyW]
GO

/****** Object:  Table [dbo].[WW_MainfreightDetail_Custom]    Script Date: 28/10/2016 3:41:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[WW_MainfreightDetail_Custom](
	[ConsignmentNo] [varchar](50) NOT NULL,
	[DetailID] [int] NOT NULL,
	[SysproDispatchNote] [varchar](20) NOT NULL,
	[SysproSalesOrder] [varchar](20) NOT NULL,
	[Items] [decimal](10, 0) NULL,
	[Description] [varchar](50) NULL,
	[CubicValue] [decimal](10, 2) NULL,
	[CubicUnit] [varchar](2) NULL,
	[WeightValue] [decimal](10, 0) NULL,
	[WeightUnit] [varchar](2) NULL,
	[DimensionQuantity] [decimal](10, 0) NULL,
	[DimensionLength] [decimal](13, 2) NULL,
	[DimensionWidth] [decimal](13, 2) NULL,
	[DimensionHeight] [decimal](13, 2) NULL,
 CONSTRAINT [PK_WW_MainfreightDetail_Custom] PRIMARY KEY CLUSTERED 
(
	[DetailID] ASC,
	[SysproDispatchNote] ASC,
	[ConsignmentNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


