﻿Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data.SqlClient
Imports System.Linq
Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.Text
Imports System.Threading.Tasks
Imports System.Collections.ObjectModel


Public Class SQLComms
    Public Property ErrorMessage() As String
        Get
            Return m_ErrorMessage
        End Get
        Set(value As String)
            m_ErrorMessage = value
        End Set
    End Property
    Private m_ErrorMessage As String

    ''' <summary>
    ''' used to set the sql connection string
    ''' </summary>
    Private strsysproconnectionstring As String = ""
    '''
    Public Property strSysproCompanyConnectionString() As String
        Get
            Return strsysproconnectionstring
        End Get
        Set(value As String)
            strsysproconnectionstring = value
        End Set
    End Property
    ' ''' <summary>
    ' ''' MAPS any sql result to a matching object
    ' ''' </summary>
    ' ''' <typeparam name="T">Object</typeparam>
    ' ''' <param name="reader">SQL datarow</param>
    ' ''' <returns>Object populated with sql results</returns>
    'Private Function MapToClass(Of T As Class)(reader As SqlDataReader) As T
    '    Dim returnedObject As T = Activator.CreateInstance(Of T)()
    '    Try
    '        Dim modelProperties As List(Of PropertyInfo) = returnedObject.[GetType]().GetProperties().OrderBy(Function(p) p.MetadataToken).ToList()
    '        For i As Integer = 0 To modelProperties.Count - 1
    '            modelProperties(i).SetValue(returnedObject, Convert.ChangeType(reader.GetValue(i), modelProperties(i).PropertyType), Nothing)
    '        Next
    '    Catch ex As Exception
    '        ErrorMessage = ex.Message
    '    End Try
    '    Return returnedObject
    'End Function

    ''' <summary>
    ''' MAPS any sql result to a matching object
    ''' </summary>
    ''' <typeparam name="T">Object</typeparam>
    ''' <param name="reader">SQL datarow</param>
    ''' <returns>Object populated with sql results</returns>
    Private Function MapToClass(Of T As Class)(reader As DataRow) As T
        Dim returnedObject As T = Activator.CreateInstance(Of T)()
        Try
            Dim columns As Integer = reader.Table.Columns.Count - 1
            Dim modelProperties As List(Of PropertyInfo) = returnedObject.[GetType]().GetProperties().OrderBy(Function(p) p.MetadataToken).ToList()
            For i As Integer = 0 To modelProperties.Count - 1
                If (columns) >= i Then
                    If Not String.IsNullOrEmpty(reader(i).ToString) Then
                        modelProperties(i).SetValue(returnedObject, Convert.ChangeType(reader(i), modelProperties(i).PropertyType), Nothing)
                    End If

                End If

            Next
        Catch ex As Exception
            ErrorMessage = ex.Message
        End Try
        Return returnedObject
    End Function

    'Private Function PopulateClassList(Of T As Class)(sqlData As SqlDataReader) As ObservableCollection(Of T)
    '    Dim returnList As ObservableCollection(Of T) = Activator.CreateInstance(Of ObservableCollection(Of T))()
    '    If sqlData.HasRows Then
    '        While sqlData.Read()

    '            Dim returnedObject = MapToClass(Of T)(sqlData)
    '            returnList.Add(returnedObject)
    '        End While
    '    End If

    '    Return returnList
    'End Function

    Private Function PopulateClassList(Of T As Class)(sqlData As DataTable) As ObservableCollection(Of T)
        Dim returnList As ObservableCollection(Of T) = Activator.CreateInstance(Of ObservableCollection(Of T))()

        For Each dr As DataRow In sqlData.Rows


            Dim returnedObject = MapToClass(Of T)(dr)
            returnList.Add(returnedObject)

        Next

        Return returnList
    End Function


    ''' <summary>
    ''' This method runs a sql query and populates a Observable collection for the specified object type
    ''' </summary>
    ''' <typeparam name="T">Object type you want to be returned </typeparam>
    ''' <param name="query">Query string to return values</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function LoadFromSql(Of T As Class)(query As String) As ObservableCollection(Of T)

        Dim connection As New SqlConnection(strSysproCompanyConnectionString)

        Using connection
            Dim command As New SqlCommand(query, connection)
            connection.Open()

            'Dim reader As SqlDataReader = command.ExecuteReader()
            Dim sqlAdap As SqlDataAdapter = New SqlDataAdapter(query, connection)
            Dim table As New DataTable
            sqlAdap.Fill(table)
            Dim sqlList As ObservableCollection(Of T) = PopulateClassList(Of T)(table) '(reader)

            'reader.Close()
            connection.Close()

            Return sqlList
        End Using
    End Function


    ''' <summary>
    ''' This is to execute update,insert or stored procedures that dont return any values
    ''' </summary>
    ''' <param name="query">a string with your sql query</param>
    ''' <param name="param">a dictionary with the sql parameter name and value as a key pair</param>
    ''' <remarks></remarks>
    Public Sub ExecuteQuery(query As String, param As Dictionary(Of String, String))
        Dim connection As New SqlConnection(strSysproCompanyConnectionString)

        Using connection
            Dim command As New SqlCommand(query, connection)

            For Each item As KeyValuePair(Of String, String) In param

                command.Parameters.AddWithValue(item.Key, item.Value)
            Next

            connection.Open()

            command.ExecuteNonQuery()

            connection.Close()
        End Using
    End Sub

    ''' <summary>
    ''' This method returns a single string value
    ''' </summary>
    ''' <param name="query">the query to return a single value</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ExecuteScalarQuery(query As String) As String
        Dim connection As New SqlConnection(strSysproCompanyConnectionString)
        Dim value As String = ""
        Using connection
            Dim command As New SqlCommand(query, connection)
            connection.Open()

            value = command.ExecuteScalar().ToString()

            connection.Close()
        End Using

        Return value
    End Function
End Class
