﻿Imports System
Imports System.Configuration
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.Collections.Specialized
Imports System.Text


Public Class BusinessLogic
    Implements INotifyPropertyChanged, INotifyCollectionChanged
#Region "Properties"
    Public Property NewDispatchNotes As ObservableCollection(Of DispatchNotes)
    Private linkdispatch As ObservableCollection(Of DispatchNotes) = New ObservableCollection(Of DispatchNotes)
    ''' <summary>
    ''' This list will display all the dispatchnotes to be used when creating a new consignment. This list will trigger a property change event when changed.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property LinkDispatchNotes As ObservableCollection(Of DispatchNotes)
        Get
            Return linkdispatch
        End Get
        Set(value As ObservableCollection(Of DispatchNotes))
            linkdispatch = value
            ' Call OnPropertyChanged whenever the property is updated
            If value.Count > 0 Then

                OnCollectionChanged(NotifyCollectionChangedAction.Reset)
            End If
        End Set
    End Property


    Public Property DefaultWarehouse As String

    Public Property Mergeconsignment As ObservableCollection(Of WW_MainfreightHeader_Custom) = New ObservableCollection(Of WW_MainfreightHeader_Custom)
    ''' <summary>
    ''' The list of consignments to be merged
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property MergeConsignmentItems As ObservableCollection(Of WW_MainfreightHeader_Custom)
        Get
            Return Mergeconsignment
        End Get
        Set(value As ObservableCollection(Of WW_MainfreightHeader_Custom))
            Mergeconsignment = value
            ' Call OnPropertyChanged whenever the property is updated
            If value.Count > 0 Then

                OnCollectionChanged(NotifyCollectionChangedAction.Reset)
            End If
        End Set
    End Property



    Public Property NewConsignments As ObservableCollection(Of WW_MainfreightHeader_Custom)
    Public Property NewConsignmentDetails As ObservableCollection(Of WW_MainfreightDetail_Custom)
    ''' <summary>
    ''' This is the list of consignments that have not been processed and is available for merging
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ConsignmentItems As ObservableCollection(Of WW_MainfreightHeader_Custom)

    ''' <summary>
    ''' This is the list of consignments that have not been processed and is available for finalisation
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FinalConsignments As ObservableCollection(Of WW_MainfreightHeader_CustomSelect)
    Public Property FinalConsignmentsFull As ObservableCollection(Of WW_MainfreightHeader_CustomSelect)


    Public Property ConsignmentDetails As WW_MainfreightDetail_Custom
    ''' <summary>
    ''' This list displays the Consignment detail lines once the create button is selected n the Create Consignment tab
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ConsignmentItemsDetail As ObservableCollection(Of WW_MainfreightDetail_Custom)

    ''' <summary>
    ''' This list is the list of customers that is displayed in the customer lookup and will be the filtered list
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CustomerList As ObservableCollection(Of Customer)

    ''' <summary>
    ''' This list contains the un-filtered customer list to be used when displaying the lookup screen and the search criteria is removed.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CustomerFullList As ObservableCollection(Of Customer)


    ''' <summary>
    ''' This list is the list of warehouses that is displayed in the warehouse lookup and will be the filtered list
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property WarehouseList As ObservableCollection(Of Warehouse)

    ''' <summary>
    ''' This list is the list of dispatch that is displayed in the dispatch note lookup and will be the filtered list
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property DispatchList As ObservableCollection(Of DispatchNotes)


    ''' <summary>
    ''' This list contains the un-filtered warehouse list to be used when displaying the lookup screen and the search criteria is removed.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property WarehouseFullList As ObservableCollection(Of Warehouse)

    ''' <summary>
    ''' This list is the list of shipping INstructions that is displayed in the shipping instruction lookup and will be the filtered list
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ShippingInstructionFullList As ObservableCollection(Of ShippingInstructions)

    ''' <summary>
    ''' This list is the list of shipping INstructions that is displayed in the shipping instruction lookup and will be the filtered list
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ShippingInstructionList As ObservableCollection(Of ShippingInstructions)

    Private selectcustomer As Customer
    ''' <summary>
    ''' This is where the customer value is stored once a customer is selected from the lookup screen
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property SelectedCustomer As Customer
        Get
            Return selectcustomer
        End Get
        Set(ByVal value As Customer)
            selectcustomer = value
            ' Call OnPropertyChanged whenever the property is updated
            OnPropertyChanged("selectcustomer")
        End Set
    End Property

    Private selectwarehouse As Warehouse
    ''' <summary>
    ''' This is where the warehouse is stored once selected in the Lookup screen
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property SelectedWarehouse As Warehouse
        Get
            Return selectwarehouse
        End Get
        Set(ByVal value As Warehouse)
            selectwarehouse = value
            ' Call OnPropertyChanged whenever the property is updated
            OnPropertyChanged("selectwarehouse")
        End Set
    End Property

    Private selectinstruction As ShippingInstructions
    ''' <summary>
    ''' This is where the ShippingInstructions is stored once selected in the Lookup screen
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property SelectedShippingInstruction As ShippingInstructions
        Get
            Return selectinstruction
        End Get
        Set(ByVal value As ShippingInstructions)
            selectinstruction = value
            ' Call OnPropertyChanged whenever the property is updated
            OnPropertyChanged("selectinstruction")
        End Set
    End Property

    Private selectdispatchnote As DispatchNotes
    Public Property SelectedDispatchNote As DispatchNotes
        Get
            Return selectdispatchnote
        End Get
        Set(ByVal value As DispatchNotes)
            selectdispatchnote = value
            ' Call OnPropertyChanged whenever the property is updated
            OnPropertyChanged("linkadd")
        End Set
    End Property

    Private selectconsignmentdetail As New WW_MainfreightDetail_Custom
    Public Property SelectedConsignmentDetail As WW_MainfreightDetail_Custom
        Get
            Return selectconsignmentdetail
        End Get
        Set(ByVal value As WW_MainfreightDetail_Custom)
            selectconsignmentdetail = value
            ' Call OnPropertyChanged whenever the property is updated
            OnPropertyChanged("detailselect")
        End Set
    End Property

    Private selectconsignment As WW_MainfreightHeader_Custom
    Public Property SelectedConsignment As WW_MainfreightHeader_Custom
        Get
            Return selectconsignment
        End Get
        Set(ByVal value As WW_MainfreightHeader_Custom)
            selectconsignment = value
            ' Call OnPropertyChanged whenever the property is updated
            OnPropertyChanged("mergeadd")
        End Set
    End Property

    Private sqlfunctions As New SQLComms

#End Region
    ''' <summary>
    ''' ON start of the application the connection string is set and the customer and warehouse lists are populated with all the required information for the lookup screens.
    ''' </summary>
    ''' <remarks></remarks>
    Sub New()
        Try
            sqlfunctions.strSysproCompanyConnectionString = ConfigurationManager.ConnectionStrings("SYSPRO").ToString

            DefaultWarehouse = My.Settings.DefaultWarehouse

            CustomerList = sqlfunctions.LoadFromSql(Of Customer)("Select Customer,Name From ArCustomer")
            WarehouseList = sqlfunctions.LoadFromSql(Of Warehouse)("Select Warehouse,Description From InvWhControl")
            ShippingInstructionList = sqlfunctions.LoadFromSql(Of ShippingInstructions)("Select Distinct ShippingInstrsCod as ShippingInstruction From MdnMasterRep with (NOLOCK)")
            FinalConsignments = sqlfunctions.LoadFromSql(Of WW_MainfreightHeader_CustomSelect)("Select 0 As Selected,* from WW_MainfreightHeader_Custom Where Processed = 0")

            'FinalConsignmentsFull = FinalConsignments
            CustomerFullList = CustomerList
            WarehouseFullList = WarehouseList
            ShippingInstructionFullList = ShippingInstructionList

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

#Region "Subs"

    ''' <summary>
    ''' Filters the customer list based on the value match for either the code or the description
    ''' </summary>
    ''' <param name="filtertext">free text</param>
    ''' <remarks></remarks>
    Public Sub FilterCustomerList(filtertext As String)
        Try

            Dim myList = CustomerFullList.Where(Function(o) o.Customer.Contains(filtertext) Or o.Name.Contains(filtertext))
            CustomerList = New ObservableCollection(Of Customer)
            For Each itm As Customer In myList
                CustomerList.Add(itm)
            Next
        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Filters the warehouse list based on the value match for either the code or the description
    ''' </summary>
    ''' <param name="filtertext">free text</param>
    ''' <remarks></remarks>
    Public Sub FilterWarehouseList(filtertext As String)
        Try

            Dim myList = WarehouseFullList.Where(Function(o) o.Warehouse.Contains(filtertext) Or o.Description.Contains(filtertext))
            WarehouseList = New ObservableCollection(Of Warehouse)
            For Each itm As Warehouse In myList
                WarehouseList.Add(itm)
            Next


        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Filters the warehouse list based on the value match for either the code or the description
    ''' </summary>
    ''' <param name="filtertext">free text</param>
    ''' <remarks></remarks>
    Public Sub FilterDispatchNoteList(filtertext As String)
        Try

            Dim myList = NewDispatchNotes.Where(Function(o) o.DispatchNote.Contains(filtertext) Or o.SYSPROSalesOrder.Contains(filtertext))
            DispatchList = New ObservableCollection(Of DispatchNotes)
            For Each itm As DispatchNotes In myList
                DispatchList.Add(itm)
            Next


        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Filters the warehouse list based on the value match for either the code or the description
    ''' </summary>
    ''' <param name="filtertext">free text</param>
    ''' <remarks></remarks>
    Public Sub FilterShippingInstructionList(filtertext As String)
        Try

            Dim myList = ShippingInstructionFullList.Where(Function(o) o.ShippingInstruction.Contains(filtertext))
            ShippingInstructionList = New ObservableCollection(Of ShippingInstructions)
            For Each itm As ShippingInstructions In myList
                ShippingInstructionList.Add(itm)
            Next


        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Public Function GetPO(dispatch As String) As String
        Try

            Dim po As String

            po = sqlfunctions.ExecuteScalarQuery("Select dbo.fWWGetPO('" + dispatch + "')")

            Return po
        Catch ex As Exception
            Return ex.Message
        End Try



    End Function

    Public Function GetCustomer(dispatch As String) As String
        Try

            Dim po As String

            po = sqlfunctions.ExecuteScalarQuery("Select dbo.fWWGetCustomer('" + dispatch + "')")

            Return po
        Catch ex As Exception
            Return ex.Message
        End Try



    End Function

    Public Function GetDispatchLines(dispatch As String) As String
        Try

            Dim po As String

            po = sqlfunctions.ExecuteScalarQuery("Select dbo.fWWGetDispatchLines('" + dispatch + "')")

            Return po
        Catch ex As Exception
            Return ex.Message
        End Try



    End Function

#Region "Create Consignment"

    ''' <summary>
    ''' Removed a selected ityem from the New DispatchNote list and adds the item to the Dispatch notes to link list
    ''' </summary>
    ''' <param name="selecteddispatch">the selected dispatch note in the new dispatch list</param>
    ''' <remarks></remarks>
    Public Sub ResetDispatchList(selecteddispatch As DispatchNotes)
        Try
            Dim DispatchNotetodel As New DispatchNotes

            For Each dispatchitem In NewDispatchNotes
                If dispatchitem.SYSPROSalesOrder = selecteddispatch.SYSPROSalesOrder And dispatchitem.DispatchNote = selecteddispatch.DispatchNote Then
                    DispatchNotetodel = dispatchitem
                End If
            Next

            NewDispatchNotes.Remove(DispatchNotetodel)
        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try

    End Sub


    ''' <summary>
    ''' Clears the Dispatch Notes to link list and adds the item back to the new Dispatch List
    ''' </summary>
    ''' <param name="selecteddispatch">Selected item in the Dispatch Notes to link list</param>
    ''' <remarks></remarks>
    Public Sub RemoveDispatchLink(selecteddispatch As DispatchNotes)
        Try
            Dim DispatchNotetodel As New DispatchNotes

            For Each dispatchitem In LinkDispatchNotes
                If dispatchitem.SYSPROSalesOrder = selecteddispatch.SYSPROSalesOrder And dispatchitem.DispatchNote = selecteddispatch.DispatchNote Then
                    DispatchNotetodel = dispatchitem
                End If
            Next

            NewDispatchNotes.Add(selecteddispatch)
            LinkDispatchNotes.Remove(DispatchNotetodel)

        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try
    End Sub




    ''' <summary>
    ''' Fills the list of New dispatchnotes. These are dispatch notes that are not linked to any consignment
    ''' </summary>
    ''' <param name="Customer">Syspro Customer</param>
    ''' <param name="Warehouse">Syspro Warehouse</param>
    ''' <param name="Instr">Syspro Shipping INstruction against ArCustomer</param>
    ''' <remarks></remarks>
    Public Sub BuildNewDispatchNotes(Customer As String, Warehouse As String, Instr As String)
        Try
            NewDispatchNotes = sqlfunctions.LoadFromSql(Of DispatchNotes)("exec prWWGetDispatchNotes '" + Customer + "','" + Warehouse + "','" + Instr + "'")
        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try


    End Sub


    Public Function CheckCustomer() As Boolean
        Dim valid As Boolean = True

        Dim customer As String
        Dim Existcustomer As String

        customer = ""

        For Each item As WW_MainfreightHeader_CustomSelect In FinalConsignments.Where(Function(w) w.Selected = True)
            If customer = "" Then
                customer = sqlfunctions.ExecuteScalarQuery("Select dbo.fWWGetCustomerCode('" + item.SenderReference + "')")
            Else
                Existcustomer = sqlfunctions.ExecuteScalarQuery("Select dbo.fWWGetCustomerCode('" + item.SenderReference + "')")
                If customer <> Existcustomer Then
                    valid = False
                End If

            End If
        Next

        Return valid

    End Function

    Public Function CheckConsignmentCustomer(conscustomer As String) As Boolean
        Dim valid As Boolean = False

        Dim customer As String
        Dim Existcustomer As String

        customer = conscustomer

        If MergeConsignmentItems.Count > 0 Then
            Existcustomer = MergeConsignmentItems(0).SysproCustomer
            If Existcustomer = customer Then
                valid = True
            Else
                valid = False
            End If
        Else
            valid = True
        End If
        Return valid

    End Function

    ''' <summary>
    ''' Created a new Consignment header and detail and clears the DispatchNotes to link list.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub CreateConsignment()
        Try
            Dim FirstDispatch = LinkDispatchNotes(0).DispatchNote

            Dim ConsignmentNo = sqlfunctions.ExecuteScalarQuery("exec prWWCeateConsignmentHeader '" + FirstDispatch + "','" + SelectedWarehouse.Warehouse + "' ")
            Dim detailid As Integer = 0
            For Each dispatch As DispatchNotes In LinkDispatchNotes
                Dim params As New Dictionary(Of String, String)
                detailid += 1
                Dim qry As New StringBuilder
                qry.AppendLine("Insert Into WW_MainfreightDetail_Custom")
                qry.AppendLine("(ConsignmentNo")
                qry.AppendLine(",DetailID,SysproDispatchNote,SysproSalesOrder")
                qry.AppendLine(",Items,Description,CubicValue,CubicUnit,WeightValue")
                qry.AppendLine(",WeightUnit,DimensionQuantity,DimensionLength")
                qry.AppendLine(",DimensionWidth,DimensionHeight)")
                qry.AppendLine(" Select @ConsignmentNo,@DetailID,@SysproDispatchNote,@SysproSalesOrder")
                qry.AppendLine(",@Item,@Description,@CubicValue,@CubicUnit,@WeightValue")
                qry.AppendLine(",@WeightUnit,@DimensionQuantity,@DimensionLength")
                qry.AppendLine(",@DimensionWidth,@DimensionHeight")

                params.Add("@ConsignmentNo", ConsignmentNo)
                params.Add("@DetailID", detailid.ToString)
                params.Add("@SysproDispatchNote", dispatch.DispatchNote)
                params.Add("@SysproSalesOrder", dispatch.SYSPROSalesOrder)
                params.Add("@Item", dispatch.Items.ToString)
                params.Add("@Description", dispatch.Description.ToString)
                params.Add("@CubicValue", dispatch.CubicValue.ToString)
                params.Add("@CubicUnit", dispatch.CubicUnit.ToString)
                params.Add("@WeightValue", dispatch.WeightValue.ToString)
                params.Add("@WeightUnit", dispatch.WeightUnit.ToString)
                params.Add("@DimensionQuantity", dispatch.DimensionQuantity.ToString)
                params.Add("@DimensionLength", dispatch.DimensionLength.ToString)
                params.Add("@DimensionWidth", dispatch.DimensionWidth.ToString)
                params.Add("@DimensionHeight", dispatch.DimensionHeight.ToString)

                Call sqlfunctions.ExecuteQuery(qry.ToString, params)


            Next


            Dim qryupdatedetail As New StringBuilder
            qryupdatedetail.AppendLine("exec prWWUpdateDetailValues '" + ConsignmentNo + "'")

            Call sqlfunctions.ExecuteQuery(qryupdatedetail.ToString, New Dictionary(Of String, String))
            Dim qryupdate As New StringBuilder
            qryupdate.AppendLine("exec prWWUpdateHeaderTotals '" + ConsignmentNo + "'")

            Call sqlfunctions.ExecuteQuery(qryupdate.ToString, New Dictionary(Of String, String))

            ConsignmentItemsDetail = sqlfunctions.LoadFromSql(Of WW_MainfreightDetail_Custom)("select * from dbo.fWWGetFreightDetails('" + ConsignmentNo + "')")

        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try


    End Sub

    Public Sub UpdateDetailValues(Consignment As String)

        Dim qryupdatedetail As New StringBuilder
        qryupdatedetail.AppendLine("exec prWWUpdateDetailValues '" + Consignment + "'")
        Call sqlfunctions.ExecuteQuery(qryupdatedetail.ToString, New Dictionary(Of String, String))
    End Sub


#End Region

#Region "Merge Consignment"
    ''' <summary>
    ''' Queries the databse for all consignments where the customer and warehouse is equal to the selected values and the consignment has not been processed
    ''' </summary>
    ''' <param name="Customer">Syspro Customer</param>
    ''' <param name="Warehouse">Syspro Warehouse</param>
    ''' <remarks></remarks>
    Public Sub BuildConsignments(Customer As String, Warehouse As String, grouped As Integer)
        Try
            If grouped = 1 Then
                ConsignmentItems = sqlfunctions.LoadFromSql(Of WW_MainfreightHeader_Custom)("Select * from WW_MainfreightHeader_Custom Where SysproCustomer like '%" + Customer + "%' and SysproWarehouse like '%" + Warehouse + "%' and Processed = 0 ")
            Else
                ConsignmentItems = sqlfunctions.LoadFromSql(Of WW_MainfreightHeader_Custom)("Select * from WW_MainfreightHeader_Custom Where SysproCustomer like '%" + Customer + "%' and SysproWarehouse like '%" + Warehouse + "%' and Processed = 0 and Grouped = " + grouped.ToString)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try


    End Sub

    ''' <summary>
    ''' Copies a selected record from the Consignments list to the Consignments to merge list
    ''' </summary>
    ''' <param name="selectedconsignment">The selected consignment in the consignment gridview</param>
    ''' <remarks></remarks>
    Public Sub ResetConsignmentListList(selectedconsignment As WW_MainfreightHeader_Custom)
        Try
            Dim Consignmenttodel As New WW_MainfreightHeader_Custom

            For Each consignmentitem In ConsignmentItems
                If consignmentitem.ConsignmentNo = selectedconsignment.ConsignmentNo And consignmentitem.SysproWarehouse = selectedconsignment.SysproWarehouse And consignmentitem.SysproCustomer = selectedconsignment.SysproCustomer Then
                    Consignmenttodel = consignmentitem
                    MergeConsignmentItems.Add(Consignmenttodel)
                End If
            Next

            ConsignmentItems.Remove(Consignmenttodel)
        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Removes a selected consignment from the to merge list and adds it back in the consignment list.
    ''' </summary>
    ''' <param name="selectedconsignment">Selected consignment in the consignment to merge list</param>
    ''' <remarks></remarks>
    Public Sub RemoveConsignmentLink(selectedconsignment As WW_MainfreightHeader_Custom)
        Try
            Dim Consignmenttodel As New WW_MainfreightHeader_Custom

            For Each consignmentitem In MergeConsignmentItems
                If consignmentitem.ConsignmentNo = selectedconsignment.ConsignmentNo And consignmentitem.SysproWarehouse = selectedconsignment.SysproWarehouse And consignmentitem.SysproCustomer = selectedconsignment.SysproCustomer Then
                    Consignmenttodel = consignmentitem
                End If
            Next

            ConsignmentItems.Add(Consignmenttodel)
            MergeConsignmentItems.Remove(Consignmenttodel)

        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub MergeConsignments()
        Try
            Dim FirstConsignment = FinalConsignments.Where(Function(o) o.Selected = True)(0) 'MergeConsignmentItems(0)
            Dim UpdConsignmentNo = FirstConsignment.ConsignmentNo
            Dim UpdSysproWarehouse = FirstConsignment.SysproWarehouse
            Dim UpdSysproCustomer = FirstConsignment.SysproCustomer

            For Each consignment As WW_MainfreightHeader_CustomSelect In FinalConsignments.Where(Function(o) o.Selected = True)  'MergeConsignmentItems
                If consignment.ConsignmentNo <> UpdConsignmentNo Then
                    Dim Sql As String
                    Sql = "Update WW_MainfreightDetail_Custom Set ConsignmentNo = '" + UpdConsignmentNo + "', DetailID = DetailID + 1000 Where ConsignmentNo = '" + consignment.ConsignmentNo + "'"

                    sqlfunctions.ExecuteQuery(Sql, New Dictionary(Of String, String))

                    Dim deletesql As String
                    deletesql = "Delete from WW_MainfreightHeader_Custom Where ConsignmentNo = '" + consignment.ConsignmentNo + "'"

                    sqlfunctions.ExecuteQuery(deletesql, New Dictionary(Of String, String))

                End If
            Next

            NewConsignmentDetails = sqlfunctions.LoadFromSql(Of WW_MainfreightDetail_Custom)("Select * from WW_MainfreightDetail_Custom Where ConsignmentNo = '" + UpdConsignmentNo + "'")
            Try
                Dim detailid As Integer = 1

                For Each detailine As WW_MainfreightDetail_Custom In NewConsignmentDetails.OrderBy(Function(o) o.DetailID)

                    sqlfunctions.ExecuteQuery("Update WW_MainfreightDetail_Custom Set DetailID = " + detailid.ToString + " Where ConsignmentNo = '" + detailine.ConsignmentNo + "' and SysproDispatchNote = '" + detailine.SysproDispatchNote + "' and DetailID = " + detailine.DetailID.ToString, New Dictionary(Of String, String))
                    detailid += 1

                Next
            Catch ex As Exception
                MessageBox.Show("Could not update DetailID: " + ex.Message)
            End Try

            Try

                UpdateDetailValues(UpdConsignmentNo)

                Dim qryupdate As New StringBuilder
                qryupdate.AppendLine("exec prWWUpdateHeaderTotals '" + UpdConsignmentNo + "'")

                sqlfunctions.ExecuteQuery(qryupdate.ToString, New Dictionary(Of String, String))
            Catch ex As Exception
                MessageBox.Show("Could not update Totals: " + ex.Message)

            End Try


            'NewConsignmentDetails = sqlfunctions.LoadFromSql(Of WW_MainfreightDetail_Custom)("Select * from WW_MainfreightDetail_Custom Where ConsignmentNo = '" + UpdConsignmentNo + "'")


        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub UpdateConsignmentDetail(DetailLine As WW_MainfreightDetail_Custom, UpdConsignmentNo As String)
        Try

            Dim qryupdate As New StringBuilder
            qryupdate.AppendLine("Update WW_MainfreightDetail_Custom")
            qryupdate.AppendLine(" Set Items = " + DetailLine.Items.ToString + " , Description = '" + DetailLine.Description + "',CubicValue = " + DetailLine.CubicValue.ToString)
            qryupdate.AppendLine(" ,CubicUnit = '" + DetailLine.CubicUnit + "', WeightValue = " + DetailLine.WeightValue.ToString + ",WeightUnit = '" + DetailLine.WeightUnit + "'")
            qryupdate.AppendLine(" , DimensionQuantity = " + DetailLine.DimensionQuantity.ToString + ",DimensionLength = " + DetailLine.DimensionLength.ToString + ",DimensionWidth = " + DetailLine.DimensionWidth.ToString + ",DimensionHeight = " + DetailLine.DimensionHeight.ToString)
            qryupdate.AppendLine(" Where ConsignmentNo = '" + UpdConsignmentNo + "' and DetailID = " + DetailLine.DetailID.ToString)

            sqlfunctions.ExecuteQuery(qryupdate.ToString, New Dictionary(Of String, String))

        Catch ex As Exception
            MessageBox.Show("Could not update Detail item: " + ex.Message)

        End Try

        Try
            Dim qryupdate As New StringBuilder
            qryupdate.AppendLine("Update WW_MainfreightHeader_Custom")
            qryupdate.AppendLine("Set TotalItems = (Items),TotalCubic = (d.CubicValue),TotalWeight = (d.WeightValue),Grouped = 1")
            qryupdate.AppendLine("From WW_MainfreightHeader_Custom h")
            qryupdate.AppendLine("Join (Select ConsignmentNo,SUM(Items) as Items,SUM(CubicValue) as CubicValue,SUM(WeightValue) as WeightValue From WW_MainfreightDetail_Custom group by ConsignmentNo) d on h.ConsignmentNo = d.ConsignmentNo")
            qryupdate.AppendLine("Where h.ConsignmentNo = '" + UpdConsignmentNo + "'")

            sqlfunctions.ExecuteQuery(qryupdate.ToString, New Dictionary(Of String, String))
        Catch ex As Exception
            MessageBox.Show("Could not update Totals: " + ex.Message)

        End Try
    End Sub

#End Region

    Public Function PopulateAllUnProcessedConsignments(Selected As Boolean, Warehouse As String, Shipping As String, Grouping As Integer)
        Dim sel As Int32

        If Selected Then
            sel = 1
        Else
            sel = 0
        End If

        FinalConsignments = sqlfunctions.LoadFromSql(Of WW_MainfreightHeader_CustomSelect)("exec prWWGetConsignments '" + sel.ToString + "','" + Warehouse + "','" + Shipping + "'," + Grouping.ToString)


        Return FinalConsignments
    End Function


    Public Function GetConsignmentDetails(Consignment As String)


        ConsignmentItemsDetail = sqlfunctions.LoadFromSql(Of WW_MainfreightDetail_Custom)("select * from dbo.fWWGetFreightDetails('" + Consignment + "')")


        Return ConsignmentItemsDetail
    End Function



    Public Function GetConsignmentFinalXML(Consigments As String)

        Dim ConsignmentXML As String
        ConsignmentXML = ""
        ConsignmentXML = sqlfunctions.ExecuteScalarQuery("exec prWWCreateConsignmentFinalXML '" + Consigments + "'")

        Return ConsignmentXML

    End Function

#End Region

#Region "Events"
    ' Declare the event
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    ' Create the OnPropertyChanged method to raise the event
    Protected Sub OnPropertyChanged(ByVal name As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(name))
    End Sub

    Public Event CollectionChanged As NotifyCollectionChangedEventHandler Implements INotifyCollectionChanged.CollectionChanged


    Protected Sub OnCollectionChanged(action As NotifyCollectionChangedAction)

        If action = 4 Then

            RaiseEvent CollectionChanged(Me, New NotifyCollectionChangedEventArgs(action))
        End If

    End Sub
#End Region


End Class
