﻿Public Class CustomerListForm

    Private logicobject As BusinessLogic


    Sub New(buslogic As BusinessLogic)
        Try
            InitializeComponent()

            CustomerDataGridView.DataSource = buslogic.CustomerFullList

            logicobject = buslogic

        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub SearchTextBox_TextChanged(sender As Object, e As EventArgs) Handles SearchTextBox.TextChanged
        Try

            logicobject.FilterCustomerList(SearchTextBox.Text)
            CustomerDataGridView.DataSource = logicobject.CustomerList
        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub CustomerDataGridView_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles CustomerDataGridView.CellDoubleClick
        Try
            If CustomerDataGridView.SelectedRows.Count > 0 Then
                Dim CustomerCode = CustomerDataGridView.SelectedRows(0).Cells(0).Value.ToString()
                Dim CustomerName = CustomerDataGridView.SelectedRows(0).Cells(1).Value.ToString()

                Dim CustomerSelect As New Customer

                CustomerSelect.Customer = CustomerCode
                CustomerSelect.Name = CustomerName



                logicobject.SelectedCustomer = CustomerSelect

                Me.Close()

            End If

        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try

    End Sub
End Class