﻿Public Class WarehouseListForm

    Private logicobject As BusinessLogic


    Sub New(buslogic As BusinessLogic)
        Try
            InitializeComponent()

            WarehouseDataGridView.DataSource = buslogic.WarehouseFullList

            logicobject = buslogic

        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub SearchTextBox_TextChanged(sender As Object, e As EventArgs) Handles SearchTextBox.TextChanged
        Try

            logicobject.FilterWarehouseList(SearchTextBox.Text)
            WarehouseDataGridView.DataSource = logicobject.WarehouseList
        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub WarehouseDataGridView_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles WarehouseDataGridView.CellDoubleClick, WarehouseDataGridView.CellDoubleClick
        Try
            If WarehouseDataGridView.SelectedRows.Count > 0 Then
                Dim WarehouseCode = WarehouseDataGridView.SelectedRows(0).Cells(0).Value.ToString()
                Dim WarehouseName = WarehouseDataGridView.SelectedRows(0).Cells(1).Value.ToString()

                Dim WarehouseSelect As New Warehouse

                WarehouseSelect.Warehouse = WarehouseCode
                WarehouseSelect.Description = WarehouseName



                logicobject.SelectedWarehouse = WarehouseSelect

                Me.Close()

            End If

        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try

    End Sub
End Class