﻿Imports System.Collections.ObjectModel
Imports System.ComponentModel
Imports System.Text

Public Class AddDispatchNoteForm

    Private WithEvents logicobject As BusinessLogic

    Sub New(buslogic As BusinessLogic)
        Try
            ' This call is required by the designer.
            InitializeComponent()

            ' Add any initialization after the InitializeComponent() call.


            logicobject = buslogic

            DispatchNoteTextBox.Text = logicobject.SelectedConsignment.SenderReference
            'CustomerTextBox.Text = logicobject.SelectedDispatchNote.SYSPROSalesOrder
            POTextBox.Text = logicobject.GetPO(logicobject.SelectedConsignment.SenderReference)
            CustomerTextBox.Text = logicobject.GetCustomer(logicobject.SelectedConsignment.SenderReference)
            NoOfLinesTextBox.Text = logicobject.GetDispatchLines(logicobject.SelectedConsignment.SenderReference)
            KGTextBox.Text = logicobject.SelectedConsignment.TotalWeight
            With DispatchNoteListView
                .View = View.Details
                .Columns.Add("DetailID")
                .Columns.Add("SysproSalesOrder")
                .Columns.Add("SysproDispatchNote")
                .Columns.Add("Items")
                .Columns.Add("Item Description")
            End With

            logicobject.ConsignmentItemsDetail = logicobject.GetConsignmentDetails(logicobject.SelectedConsignment.ConsignmentNo)

            For Each disp As WW_MainfreightDetail_Custom In logicobject.ConsignmentItemsDetail

                If disp.ConsignmentNo = logicobject.SelectedConsignment.ConsignmentNo Then
                    Dim listitm As New ListViewItem(New String() {disp.DetailID, disp.SYSPROSalesOrder, disp.SysproDispatchNote, disp.Items, disp.Description})
                    DispatchNoteListView.Items.Add(listitm)
                End If


            Next

        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try



    End Sub



    Private Sub DispatchSaveButton_Click(sender As Object, e As EventArgs) Handles DispatchSaveButton.Click
        Try






            If ValidateControl() Then
                Dim NewDispatchItem As New WW_MainfreightDetail_Custom
                NewDispatchItem.Items = ItemsNumericUpDown.Value
                NewDispatchItem.Description = ItemDescriptionComboBox.Text
                NewDispatchItem.CubicValue = logicobject.SelectedConsignmentDetail.CubicValue
                NewDispatchItem.CubicUnit = CubicUnitTextBox.Text
                NewDispatchItem.WeightValue = logicobject.SelectedConsignmentDetail.WeightValue
                NewDispatchItem.WeightUnit = WeightUnitTextBox.Text
                NewDispatchItem.DimensionQuantity = DimensionQuantityNumericUpDown.Value
                NewDispatchItem.DimensionLength = DimensionLengthNumericUpDown.Value
                NewDispatchItem.DimensionWidth = DimensionWidthNumericUpDown.Value
                NewDispatchItem.DimensionHeight = DimensionHeightNumericUpDown.Value


                NewDispatchItem.ConsignmentNo = logicobject.SelectedConsignmentDetail.ConsignmentNo
                NewDispatchItem.SYSPROSalesOrder = logicobject.SelectedConsignmentDetail.SYSPROSalesOrder
                NewDispatchItem.SysproDispatchNote = logicobject.SelectedConsignmentDetail.SysproDispatchNote
                NewDispatchItem.DetailID = logicobject.SelectedConsignmentDetail.DetailID

                DispatchNoteListView.Items.Clear()


                For Each disp As WW_MainfreightDetail_Custom In logicobject.ConsignmentItemsDetail

                    If disp.ConsignmentNo = logicobject.SelectedConsignment.ConsignmentNo And disp.DetailID = logicobject.SelectedConsignmentDetail.DetailID Then

                        disp = NewDispatchItem

                        logicobject.UpdateConsignmentDetail(NewDispatchItem, NewDispatchItem.ConsignmentNo)


                    End If

                    Dim listitm As New ListViewItem(New String() {disp.DetailID, disp.SYSPROSalesOrder, disp.SysproDispatchNote, disp.Items, disp.Description})



                    DispatchNoteListView.Items.Add(listitm)


                Next
                logicobject.UpdateDetailValues(logicobject.SelectedConsignmentDetail.ConsignmentNo)

                
            Else
                MessageBox.Show("All the fields are required to have values")
            End If


        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try


    End Sub

    Private Function ValidateControl() As Boolean

        Dim Valid = False

        If ItemsNumericUpDown.Value = 0 Then
            Valid = False
            Exit Function
        Else
            Valid = True
        End If

        If ItemDescriptionComboBox.Text = "" Then
            Valid = False
            Exit Function
        Else
            Valid = True
        End If




        If CubicUnitTextBox.Text = "" Then
            Valid = False
            Exit Function
        Else
            Valid = True
        End If




        If WeightUnitTextBox.Text = "" Then
            Valid = False
            Exit Function
        Else
            Valid = True
        End If

        If DimensionQuantityNumericUpDown.Value = 0 Then
            Valid = False
            Exit Function
        Else
            Valid = True
        End If

        If DimensionLengthNumericUpDown.Value = 0 Then
            Valid = False
            Exit Function
        Else
            Valid = True
        End If

        If DimensionWidthNumericUpDown.Value = 0 Then
            Valid = False
            Exit Function
        Else
            Valid = True
        End If

        If DimensionHeightNumericUpDown.Value = 0 Then
            Valid = False
            Exit Function
        Else
            Valid = True
        End If



        Return Valid
    End Function

    Private Sub DispatchCancelButton_Click(sender As Object, e As EventArgs) Handles DispatchCancelButton.Click
        Me.Close()
    End Sub

    Private Sub CompleteButton_Click(sender As Object, e As EventArgs)
        logicobject.CreateConsignment()
        logicobject.ResetDispatchList(logicobject.SelectedDispatchNote)
        logicobject.LinkDispatchNotes = logicobject.LinkDispatchNotes
        Me.Close()
    End Sub



    ''' <summary>
    ''' Listener event. If properties change in the logic object the form needs to react. this event listens to the customer and warehouse properties for change.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Logic_PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Handles logicobject.PropertyChanged
        Try
            If e.PropertyName = "linkadd" Then
                DispatchNoteTextBox.Text = logicobject.SelectedDispatchNote.DispatchNote
                CustomerTextBox.Text = logicobject.SelectedDispatchNote.SYSPROSalesOrder
                POTextBox.Text = logicobject.GetPO(logicobject.SelectedDispatchNote.DispatchNote)
                CustomerTextBox.Text = logicobject.GetCustomer(logicobject.SelectedDispatchNote.DispatchNote)
                NoOfLinesTextBox.Text = logicobject.GetDispatchLines(logicobject.SelectedDispatchNote.DispatchNote)
                KGTextBox.Text = "0"

            End If
        Catch ex As Exception

        End Try
    End Sub


    Private Sub DispatchNoteListView_ItemSelectionChanged(sender As Object, e As ListViewItemSelectionChangedEventArgs) Handles DispatchNoteListView.ItemSelectionChanged
        Try
            Dim selectitm As New WW_MainfreightDetail_Custom
            selectitm.DetailID = DispatchNoteListView.SelectedItems(0).Text
            For Each itm As WW_MainfreightDetail_Custom In logicobject.ConsignmentItemsDetail
                If selectitm.DetailID = itm.DetailID Then
                    logicobject.SelectedConsignmentDetail = itm
                End If
            Next


            ItemsNumericUpDown.Value = logicobject.SelectedConsignmentDetail.Items
            ItemDescriptionComboBox.Text = logicobject.SelectedConsignmentDetail.Description
            CubicUnitTextBox.Text = logicobject.SelectedConsignmentDetail.CubicUnit
            WeightUnitTextBox.Text = logicobject.SelectedConsignmentDetail.WeightUnit
            DimensionQuantityNumericUpDown.Value = logicobject.SelectedConsignmentDetail.DimensionQuantity
            DimensionLengthNumericUpDown.Value = logicobject.SelectedConsignmentDetail.DimensionLength
            DimensionWidthNumericUpDown.Value = logicobject.SelectedConsignmentDetail.DimensionWidth
            DimensionHeightNumericUpDown.Value = logicobject.SelectedConsignmentDetail.DimensionHeight


        Catch ex As Exception

        End Try




    End Sub
End Class