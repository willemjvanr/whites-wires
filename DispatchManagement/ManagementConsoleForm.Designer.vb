﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ManagementConsoleForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ManagementConsoleForm))
        Me.ConsignmentTabControl = New System.Windows.Forms.TabControl()
        Me.CreateConsigmentTab = New System.Windows.Forms.TabPage()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.CustomerTextBox = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.DispatchCancelButton = New System.Windows.Forms.Button()
        Me.DispatchSaveButton = New System.Windows.Forms.Button()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.POTextBox = New System.Windows.Forms.TextBox()
        Me.NoOfLinesTextBox = New System.Windows.Forms.TextBox()
        Me.KGTextBox = New System.Windows.Forms.TextBox()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.FinishButton = New System.Windows.Forms.Button()
        Me.PrintLabelsButton = New System.Windows.Forms.Button()
        Me.CompleteButton = New System.Windows.Forms.Button()
        Me.DispatchNoteListView = New System.Windows.Forms.ListView()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.ItemsNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.DispatchNoteTextBox = New System.Windows.Forms.TextBox()
        Me.SearchDispatchButton = New System.Windows.Forms.Button()
        Me.ItemDescriptionComboBox = New System.Windows.Forms.ComboBox()
        Me.CubicUnitTextBox = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.WeightUnitTextBox = New System.Windows.Forms.TextBox()
        Me.DimensionQuantityNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.DimensionLengthNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.DimensionWidthNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.DimensionHeightNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.HeaderPanel = New System.Windows.Forms.Panel()
        Me.SettingsButton = New System.Windows.Forms.Button()
        Me.ShippingInstructionButton = New System.Windows.Forms.Button()
        Me.ShippingInstructionTextBox = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.WarehouseSearchButton = New System.Windows.Forms.Button()
        Me.WarehouseTextBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.FinaliseConsignmentTab = New System.Windows.Forms.TabPage()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.UnProcessedConsignmentDataGridView = New System.Windows.Forms.DataGridView()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.EditButton = New System.Windows.Forms.Button()
        Me.TotalWeightLabel = New System.Windows.Forms.Label()
        Me.TotalCubicLabel = New System.Windows.Forms.Label()
        Me.GroupingCheckBox = New System.Windows.Forms.CheckBox()
        Me.MergeConsigmentButton = New System.Windows.Forms.Button()
        Me.SelectAllCheckBox = New System.Windows.Forms.CheckBox()
        Me.ShipInstructFinaliseButton = New System.Windows.Forms.Button()
        Me.ShippingInstructFinaliseTextBox = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ProcessButton = New System.Windows.Forms.Button()
        Me.UnProcessedConsignmentWarehouseButton = New System.Windows.Forms.Button()
        Me.UnProcessedConsignmentWarehouseTextBox = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.RePrintButton = New System.Windows.Forms.Button()
        Me.ConsignmentTabControl.SuspendLayout()
        Me.CreateConsigmentTab.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel5.SuspendLayout()
        CType(Me.ItemsNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel6.SuspendLayout()
        CType(Me.DimensionQuantityNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DimensionLengthNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DimensionWidthNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DimensionHeightNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.HeaderPanel.SuspendLayout()
        Me.FinaliseConsignmentTab.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        CType(Me.UnProcessedConsignmentDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'ConsignmentTabControl
        '
        Me.ConsignmentTabControl.Controls.Add(Me.CreateConsigmentTab)
        Me.ConsignmentTabControl.Controls.Add(Me.FinaliseConsignmentTab)
        resources.ApplyResources(Me.ConsignmentTabControl, "ConsignmentTabControl")
        Me.ConsignmentTabControl.Name = "ConsignmentTabControl"
        Me.ConsignmentTabControl.SelectedIndex = 0
        '
        'CreateConsigmentTab
        '
        Me.CreateConsigmentTab.Controls.Add(Me.TableLayoutPanel1)
        resources.ApplyResources(Me.CreateConsigmentTab, "CreateConsigmentTab")
        Me.CreateConsigmentTab.Name = "CreateConsigmentTab"
        Me.CreateConsigmentTab.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel1
        '
        resources.ApplyResources(Me.TableLayoutPanel1, "TableLayoutPanel1")
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel4, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.HeaderPanel, 0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        '
        'TableLayoutPanel4
        '
        resources.ApplyResources(Me.TableLayoutPanel4, "TableLayoutPanel4")
        Me.TableLayoutPanel4.Controls.Add(Me.Label5, 0, 2)
        Me.TableLayoutPanel4.Controls.Add(Me.CustomerTextBox, 1, 2)
        Me.TableLayoutPanel4.Controls.Add(Me.Panel1, 1, 20)
        Me.TableLayoutPanel4.Controls.Add(Me.Label15, 0, 11)
        Me.TableLayoutPanel4.Controls.Add(Me.Label16, 0, 3)
        Me.TableLayoutPanel4.Controls.Add(Me.Label17, 0, 4)
        Me.TableLayoutPanel4.Controls.Add(Me.Label18, 0, 5)
        Me.TableLayoutPanel4.Controls.Add(Me.POTextBox, 1, 3)
        Me.TableLayoutPanel4.Controls.Add(Me.NoOfLinesTextBox, 1, 4)
        Me.TableLayoutPanel4.Controls.Add(Me.KGTextBox, 1, 5)
        Me.TableLayoutPanel4.Controls.Add(Me.Panel5, 1, 21)
        Me.TableLayoutPanel4.Controls.Add(Me.DispatchNoteListView, 0, 6)
        Me.TableLayoutPanel4.Controls.Add(Me.Label19, 0, 10)
        Me.TableLayoutPanel4.Controls.Add(Me.ItemsNumericUpDown, 1, 10)
        Me.TableLayoutPanel4.Controls.Add(Me.Label20, 0, 1)
        Me.TableLayoutPanel4.Controls.Add(Me.Panel6, 1, 1)
        Me.TableLayoutPanel4.Controls.Add(Me.ItemDescriptionComboBox, 1, 11)
        Me.TableLayoutPanel4.Controls.Add(Me.CubicUnitTextBox, 1, 12)
        Me.TableLayoutPanel4.Controls.Add(Me.Label13, 0, 12)
        Me.TableLayoutPanel4.Controls.Add(Me.Label12, 0, 13)
        Me.TableLayoutPanel4.Controls.Add(Me.WeightUnitTextBox, 1, 13)
        Me.TableLayoutPanel4.Controls.Add(Me.DimensionQuantityNumericUpDown, 1, 14)
        Me.TableLayoutPanel4.Controls.Add(Me.Label8, 0, 14)
        Me.TableLayoutPanel4.Controls.Add(Me.Label9, 0, 15)
        Me.TableLayoutPanel4.Controls.Add(Me.Label10, 0, 16)
        Me.TableLayoutPanel4.Controls.Add(Me.Label11, 0, 17)
        Me.TableLayoutPanel4.Controls.Add(Me.DimensionLengthNumericUpDown, 1, 15)
        Me.TableLayoutPanel4.Controls.Add(Me.DimensionWidthNumericUpDown, 1, 16)
        Me.TableLayoutPanel4.Controls.Add(Me.DimensionHeightNumericUpDown, 1, 17)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.Name = "Label5"
        '
        'CustomerTextBox
        '
        resources.ApplyResources(Me.CustomerTextBox, "CustomerTextBox")
        Me.CustomerTextBox.Name = "CustomerTextBox"
        Me.CustomerTextBox.ReadOnly = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.DispatchCancelButton)
        Me.Panel1.Controls.Add(Me.DispatchSaveButton)
        resources.ApplyResources(Me.Panel1, "Panel1")
        Me.Panel1.Name = "Panel1"
        '
        'DispatchCancelButton
        '
        resources.ApplyResources(Me.DispatchCancelButton, "DispatchCancelButton")
        Me.DispatchCancelButton.Name = "DispatchCancelButton"
        Me.DispatchCancelButton.UseVisualStyleBackColor = True
        '
        'DispatchSaveButton
        '
        resources.ApplyResources(Me.DispatchSaveButton, "DispatchSaveButton")
        Me.DispatchSaveButton.Name = "DispatchSaveButton"
        Me.DispatchSaveButton.UseVisualStyleBackColor = True
        '
        'Label15
        '
        resources.ApplyResources(Me.Label15, "Label15")
        Me.Label15.Name = "Label15"
        '
        'Label16
        '
        resources.ApplyResources(Me.Label16, "Label16")
        Me.Label16.Name = "Label16"
        '
        'Label17
        '
        resources.ApplyResources(Me.Label17, "Label17")
        Me.Label17.Name = "Label17"
        '
        'Label18
        '
        resources.ApplyResources(Me.Label18, "Label18")
        Me.Label18.Name = "Label18"
        '
        'POTextBox
        '
        resources.ApplyResources(Me.POTextBox, "POTextBox")
        Me.POTextBox.Name = "POTextBox"
        Me.POTextBox.ReadOnly = True
        '
        'NoOfLinesTextBox
        '
        resources.ApplyResources(Me.NoOfLinesTextBox, "NoOfLinesTextBox")
        Me.NoOfLinesTextBox.Name = "NoOfLinesTextBox"
        Me.NoOfLinesTextBox.ReadOnly = True
        '
        'KGTextBox
        '
        resources.ApplyResources(Me.KGTextBox, "KGTextBox")
        Me.KGTextBox.Name = "KGTextBox"
        Me.KGTextBox.ReadOnly = True
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.FinishButton)
        Me.Panel5.Controls.Add(Me.PrintLabelsButton)
        Me.Panel5.Controls.Add(Me.CompleteButton)
        resources.ApplyResources(Me.Panel5, "Panel5")
        Me.Panel5.Name = "Panel5"
        '
        'FinishButton
        '
        resources.ApplyResources(Me.FinishButton, "FinishButton")
        Me.FinishButton.Name = "FinishButton"
        Me.FinishButton.UseVisualStyleBackColor = True
        '
        'PrintLabelsButton
        '
        resources.ApplyResources(Me.PrintLabelsButton, "PrintLabelsButton")
        Me.PrintLabelsButton.Name = "PrintLabelsButton"
        Me.PrintLabelsButton.UseVisualStyleBackColor = True
        '
        'CompleteButton
        '
        resources.ApplyResources(Me.CompleteButton, "CompleteButton")
        Me.CompleteButton.Name = "CompleteButton"
        Me.CompleteButton.UseVisualStyleBackColor = True
        '
        'DispatchNoteListView
        '
        Me.TableLayoutPanel4.SetColumnSpan(Me.DispatchNoteListView, 2)
        resources.ApplyResources(Me.DispatchNoteListView, "DispatchNoteListView")
        Me.DispatchNoteListView.Name = "DispatchNoteListView"
        Me.TableLayoutPanel4.SetRowSpan(Me.DispatchNoteListView, 4)
        Me.DispatchNoteListView.UseCompatibleStateImageBehavior = False
        '
        'Label19
        '
        resources.ApplyResources(Me.Label19, "Label19")
        Me.Label19.Name = "Label19"
        '
        'ItemsNumericUpDown
        '
        resources.ApplyResources(Me.ItemsNumericUpDown, "ItemsNumericUpDown")
        Me.ItemsNumericUpDown.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.ItemsNumericUpDown.Name = "ItemsNumericUpDown"
        '
        'Label20
        '
        resources.ApplyResources(Me.Label20, "Label20")
        Me.Label20.Name = "Label20"
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.DispatchNoteTextBox)
        Me.Panel6.Controls.Add(Me.SearchDispatchButton)
        resources.ApplyResources(Me.Panel6, "Panel6")
        Me.Panel6.Name = "Panel6"
        '
        'DispatchNoteTextBox
        '
        resources.ApplyResources(Me.DispatchNoteTextBox, "DispatchNoteTextBox")
        Me.DispatchNoteTextBox.Name = "DispatchNoteTextBox"
        Me.DispatchNoteTextBox.ReadOnly = True
        '
        'SearchDispatchButton
        '
        resources.ApplyResources(Me.SearchDispatchButton, "SearchDispatchButton")
        Me.SearchDispatchButton.Name = "SearchDispatchButton"
        Me.SearchDispatchButton.UseVisualStyleBackColor = True
        '
        'ItemDescriptionComboBox
        '
        Me.ItemDescriptionComboBox.FormattingEnabled = True
        Me.ItemDescriptionComboBox.Items.AddRange(New Object() {resources.GetString("ItemDescriptionComboBox.Items"), resources.GetString("ItemDescriptionComboBox.Items1"), resources.GetString("ItemDescriptionComboBox.Items2"), resources.GetString("ItemDescriptionComboBox.Items3"), resources.GetString("ItemDescriptionComboBox.Items4"), resources.GetString("ItemDescriptionComboBox.Items5")})
        resources.ApplyResources(Me.ItemDescriptionComboBox, "ItemDescriptionComboBox")
        Me.ItemDescriptionComboBox.Name = "ItemDescriptionComboBox"
        '
        'CubicUnitTextBox
        '
        resources.ApplyResources(Me.CubicUnitTextBox, "CubicUnitTextBox")
        Me.CubicUnitTextBox.Name = "CubicUnitTextBox"
        '
        'Label13
        '
        resources.ApplyResources(Me.Label13, "Label13")
        Me.Label13.Name = "Label13"
        '
        'Label12
        '
        resources.ApplyResources(Me.Label12, "Label12")
        Me.Label12.Name = "Label12"
        '
        'WeightUnitTextBox
        '
        resources.ApplyResources(Me.WeightUnitTextBox, "WeightUnitTextBox")
        Me.WeightUnitTextBox.Name = "WeightUnitTextBox"
        '
        'DimensionQuantityNumericUpDown
        '
        resources.ApplyResources(Me.DimensionQuantityNumericUpDown, "DimensionQuantityNumericUpDown")
        Me.DimensionQuantityNumericUpDown.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.DimensionQuantityNumericUpDown.Name = "DimensionQuantityNumericUpDown"
        '
        'Label8
        '
        resources.ApplyResources(Me.Label8, "Label8")
        Me.Label8.Name = "Label8"
        '
        'Label9
        '
        resources.ApplyResources(Me.Label9, "Label9")
        Me.Label9.Name = "Label9"
        '
        'Label10
        '
        resources.ApplyResources(Me.Label10, "Label10")
        Me.Label10.Name = "Label10"
        '
        'Label11
        '
        resources.ApplyResources(Me.Label11, "Label11")
        Me.Label11.Name = "Label11"
        '
        'DimensionLengthNumericUpDown
        '
        Me.DimensionLengthNumericUpDown.DecimalPlaces = 2
        resources.ApplyResources(Me.DimensionLengthNumericUpDown, "DimensionLengthNumericUpDown")
        Me.DimensionLengthNumericUpDown.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.DimensionLengthNumericUpDown.Name = "DimensionLengthNumericUpDown"
        '
        'DimensionWidthNumericUpDown
        '
        Me.DimensionWidthNumericUpDown.DecimalPlaces = 2
        resources.ApplyResources(Me.DimensionWidthNumericUpDown, "DimensionWidthNumericUpDown")
        Me.DimensionWidthNumericUpDown.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.DimensionWidthNumericUpDown.Name = "DimensionWidthNumericUpDown"
        '
        'DimensionHeightNumericUpDown
        '
        Me.DimensionHeightNumericUpDown.DecimalPlaces = 2
        resources.ApplyResources(Me.DimensionHeightNumericUpDown, "DimensionHeightNumericUpDown")
        Me.DimensionHeightNumericUpDown.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.DimensionHeightNumericUpDown.Name = "DimensionHeightNumericUpDown"
        '
        'HeaderPanel
        '
        resources.ApplyResources(Me.HeaderPanel, "HeaderPanel")
        Me.HeaderPanel.Controls.Add(Me.SettingsButton)
        Me.HeaderPanel.Controls.Add(Me.ShippingInstructionButton)
        Me.HeaderPanel.Controls.Add(Me.ShippingInstructionTextBox)
        Me.HeaderPanel.Controls.Add(Me.Label4)
        Me.HeaderPanel.Controls.Add(Me.WarehouseSearchButton)
        Me.HeaderPanel.Controls.Add(Me.WarehouseTextBox)
        Me.HeaderPanel.Controls.Add(Me.Label1)
        Me.HeaderPanel.Name = "HeaderPanel"
        '
        'SettingsButton
        '
        resources.ApplyResources(Me.SettingsButton, "SettingsButton")
        Me.SettingsButton.Name = "SettingsButton"
        Me.SettingsButton.Tag = "Settings"
        Me.SettingsButton.UseVisualStyleBackColor = True
        '
        'ShippingInstructionButton
        '
        resources.ApplyResources(Me.ShippingInstructionButton, "ShippingInstructionButton")
        Me.ShippingInstructionButton.Name = "ShippingInstructionButton"
        Me.ShippingInstructionButton.UseVisualStyleBackColor = True
        '
        'ShippingInstructionTextBox
        '
        resources.ApplyResources(Me.ShippingInstructionTextBox, "ShippingInstructionTextBox")
        Me.ShippingInstructionTextBox.Name = "ShippingInstructionTextBox"
        Me.ShippingInstructionTextBox.ReadOnly = True
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'WarehouseSearchButton
        '
        resources.ApplyResources(Me.WarehouseSearchButton, "WarehouseSearchButton")
        Me.WarehouseSearchButton.Name = "WarehouseSearchButton"
        Me.WarehouseSearchButton.UseVisualStyleBackColor = True
        '
        'WarehouseTextBox
        '
        resources.ApplyResources(Me.WarehouseTextBox, "WarehouseTextBox")
        Me.WarehouseTextBox.Name = "WarehouseTextBox"
        Me.WarehouseTextBox.ReadOnly = True
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'FinaliseConsignmentTab
        '
        Me.FinaliseConsignmentTab.Controls.Add(Me.TableLayoutPanel3)
        resources.ApplyResources(Me.FinaliseConsignmentTab, "FinaliseConsignmentTab")
        Me.FinaliseConsignmentTab.Name = "FinaliseConsignmentTab"
        Me.FinaliseConsignmentTab.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel3
        '
        resources.ApplyResources(Me.TableLayoutPanel3, "TableLayoutPanel3")
        Me.TableLayoutPanel3.Controls.Add(Me.GroupBox6, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel4, 0, 0)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        '
        'GroupBox6
        '
        Me.TableLayoutPanel3.SetColumnSpan(Me.GroupBox6, 3)
        Me.GroupBox6.Controls.Add(Me.UnProcessedConsignmentDataGridView)
        resources.ApplyResources(Me.GroupBox6, "GroupBox6")
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.TabStop = False
        '
        'UnProcessedConsignmentDataGridView
        '
        Me.UnProcessedConsignmentDataGridView.AllowUserToAddRows = False
        Me.UnProcessedConsignmentDataGridView.AllowUserToDeleteRows = False
        Me.UnProcessedConsignmentDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.UnProcessedConsignmentDataGridView, "UnProcessedConsignmentDataGridView")
        Me.UnProcessedConsignmentDataGridView.Name = "UnProcessedConsignmentDataGridView"
        '
        'Panel4
        '
        resources.ApplyResources(Me.Panel4, "Panel4")
        Me.TableLayoutPanel3.SetColumnSpan(Me.Panel4, 3)
        Me.Panel4.Controls.Add(Me.RePrintButton)
        Me.Panel4.Controls.Add(Me.EditButton)
        Me.Panel4.Controls.Add(Me.TotalWeightLabel)
        Me.Panel4.Controls.Add(Me.TotalCubicLabel)
        Me.Panel4.Controls.Add(Me.GroupingCheckBox)
        Me.Panel4.Controls.Add(Me.MergeConsigmentButton)
        Me.Panel4.Controls.Add(Me.SelectAllCheckBox)
        Me.Panel4.Controls.Add(Me.ShipInstructFinaliseButton)
        Me.Panel4.Controls.Add(Me.ShippingInstructFinaliseTextBox)
        Me.Panel4.Controls.Add(Me.Label7)
        Me.Panel4.Controls.Add(Me.ProcessButton)
        Me.Panel4.Controls.Add(Me.UnProcessedConsignmentWarehouseButton)
        Me.Panel4.Controls.Add(Me.UnProcessedConsignmentWarehouseTextBox)
        Me.Panel4.Controls.Add(Me.Label6)
        Me.Panel4.Name = "Panel4"
        '
        'EditButton
        '
        resources.ApplyResources(Me.EditButton, "EditButton")
        Me.EditButton.Name = "EditButton"
        Me.EditButton.UseVisualStyleBackColor = True
        '
        'TotalWeightLabel
        '
        resources.ApplyResources(Me.TotalWeightLabel, "TotalWeightLabel")
        Me.TotalWeightLabel.Name = "TotalWeightLabel"
        '
        'TotalCubicLabel
        '
        resources.ApplyResources(Me.TotalCubicLabel, "TotalCubicLabel")
        Me.TotalCubicLabel.Name = "TotalCubicLabel"
        '
        'GroupingCheckBox
        '
        resources.ApplyResources(Me.GroupingCheckBox, "GroupingCheckBox")
        Me.GroupingCheckBox.Name = "GroupingCheckBox"
        Me.GroupingCheckBox.UseVisualStyleBackColor = True
        '
        'MergeConsigmentButton
        '
        resources.ApplyResources(Me.MergeConsigmentButton, "MergeConsigmentButton")
        Me.MergeConsigmentButton.Name = "MergeConsigmentButton"
        Me.MergeConsigmentButton.UseVisualStyleBackColor = True
        '
        'SelectAllCheckBox
        '
        resources.ApplyResources(Me.SelectAllCheckBox, "SelectAllCheckBox")
        Me.SelectAllCheckBox.Name = "SelectAllCheckBox"
        Me.SelectAllCheckBox.UseVisualStyleBackColor = True
        '
        'ShipInstructFinaliseButton
        '
        resources.ApplyResources(Me.ShipInstructFinaliseButton, "ShipInstructFinaliseButton")
        Me.ShipInstructFinaliseButton.Name = "ShipInstructFinaliseButton"
        Me.ShipInstructFinaliseButton.UseVisualStyleBackColor = True
        '
        'ShippingInstructFinaliseTextBox
        '
        resources.ApplyResources(Me.ShippingInstructFinaliseTextBox, "ShippingInstructFinaliseTextBox")
        Me.ShippingInstructFinaliseTextBox.Name = "ShippingInstructFinaliseTextBox"
        Me.ShippingInstructFinaliseTextBox.ReadOnly = True
        '
        'Label7
        '
        resources.ApplyResources(Me.Label7, "Label7")
        Me.Label7.Name = "Label7"
        '
        'ProcessButton
        '
        resources.ApplyResources(Me.ProcessButton, "ProcessButton")
        Me.ProcessButton.Name = "ProcessButton"
        Me.ProcessButton.UseVisualStyleBackColor = True
        '
        'UnProcessedConsignmentWarehouseButton
        '
        resources.ApplyResources(Me.UnProcessedConsignmentWarehouseButton, "UnProcessedConsignmentWarehouseButton")
        Me.UnProcessedConsignmentWarehouseButton.Name = "UnProcessedConsignmentWarehouseButton"
        Me.UnProcessedConsignmentWarehouseButton.UseVisualStyleBackColor = True
        '
        'UnProcessedConsignmentWarehouseTextBox
        '
        resources.ApplyResources(Me.UnProcessedConsignmentWarehouseTextBox, "UnProcessedConsignmentWarehouseTextBox")
        Me.UnProcessedConsignmentWarehouseTextBox.Name = "UnProcessedConsignmentWarehouseTextBox"
        Me.UnProcessedConsignmentWarehouseTextBox.ReadOnly = True
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.Name = "Label6"
        '
        'RePrintButton
        '
        resources.ApplyResources(Me.RePrintButton, "RePrintButton")
        Me.RePrintButton.Name = "RePrintButton"
        Me.RePrintButton.UseVisualStyleBackColor = True
        '
        'ManagementConsoleForm
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.ConsignmentTabControl)
        Me.Name = "ManagementConsoleForm"
        Me.ConsignmentTabControl.ResumeLayout(False)
        Me.CreateConsigmentTab.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.TableLayoutPanel4.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        CType(Me.ItemsNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        CType(Me.DimensionQuantityNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DimensionLengthNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DimensionWidthNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DimensionHeightNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.HeaderPanel.ResumeLayout(False)
        Me.HeaderPanel.PerformLayout()
        Me.FinaliseConsignmentTab.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        CType(Me.UnProcessedConsignmentDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ConsignmentTabControl As System.Windows.Forms.TabControl
    Friend WithEvents CreateConsigmentTab As System.Windows.Forms.TabPage
    Friend WithEvents FinaliseConsignmentTab As System.Windows.Forms.TabPage
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents UnProcessedConsignmentDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents UnProcessedConsignmentWarehouseButton As System.Windows.Forms.Button
    Friend WithEvents UnProcessedConsignmentWarehouseTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents ProcessButton As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents HeaderPanel As System.Windows.Forms.Panel
    Friend WithEvents ShippingInstructionButton As System.Windows.Forms.Button
    Friend WithEvents ShippingInstructionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents WarehouseSearchButton As System.Windows.Forms.Button
    Friend WithEvents WarehouseTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents CustomerTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents DispatchCancelButton As System.Windows.Forms.Button
    Friend WithEvents DispatchSaveButton As System.Windows.Forms.Button
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents DimensionHeightNumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents DimensionWidthNumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents DimensionLengthNumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents DimensionQuantityNumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents WeightUnitTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CubicUnitTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents POTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NoOfLinesTextBox As System.Windows.Forms.TextBox
    Friend WithEvents KGTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents CompleteButton As System.Windows.Forms.Button
    Friend WithEvents DispatchNoteListView As System.Windows.Forms.ListView
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents ItemsNumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents DispatchNoteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SearchDispatchButton As System.Windows.Forms.Button
    Friend WithEvents ShipInstructFinaliseButton As System.Windows.Forms.Button
    Friend WithEvents ShippingInstructFinaliseTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents ItemDescriptionComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents FinishButton As System.Windows.Forms.Button
    Friend WithEvents PrintLabelsButton As System.Windows.Forms.Button
    Friend WithEvents SelectAllCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents MergeConsigmentButton As System.Windows.Forms.Button
    Friend WithEvents GroupingCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents TotalWeightLabel As System.Windows.Forms.Label
    Friend WithEvents TotalCubicLabel As System.Windows.Forms.Label
    Friend WithEvents EditButton As System.Windows.Forms.Button
    Friend WithEvents SettingsButton As System.Windows.Forms.Button
    Friend WithEvents RePrintButton As System.Windows.Forms.Button

End Class
