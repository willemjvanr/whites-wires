﻿Public Class DispatchNoteListForm

    Private logicobject As BusinessLogic

    Sub New(buslogic As BusinessLogic)
        Try
            InitializeComponent()

            DispatchNoteDataGridView.DataSource = buslogic.NewDispatchNotes

            logicobject = buslogic

        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub SearchTextBox_TextChanged(sender As Object, e As EventArgs) Handles SearchTextBox.TextChanged
        Try

            logicobject.FilterCustomerList(SearchTextBox.Text)
            DispatchNoteDataGridView.DataSource = logicobject.DispatchList
        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub CustomerDataGridView_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles DispatchNoteDataGridView.CellDoubleClick
        Try
            If DispatchNoteDataGridView.SelectedRows.Count > 0 Then
                Dim DispatchNote = DispatchNoteDataGridView.SelectedRows(0).Cells(0).Value.ToString()
                Dim SalesOrder = DispatchNoteDataGridView.SelectedRows(0).Cells(2).Value.ToString()

                Dim DispatchSelect As New DispatchNotes

                DispatchSelect.DispatchNote = DispatchNote
                DispatchSelect.SYSPROSalesOrder = SalesOrder


                logicobject.SelectedDispatchNote = DispatchSelect

                Me.Close()

            End If

        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try

    End Sub

End Class