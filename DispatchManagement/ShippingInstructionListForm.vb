﻿Public Class ShippingInstructionListForm

    Private logicobject As BusinessLogic


    Sub New(buslogic As BusinessLogic)
        Try
            InitializeComponent()

            ShippingInstructionDataGridView.DataSource = buslogic.ShippingInstructionFullList

            logicobject = buslogic

        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub SearchTextBox_TextChanged(sender As Object, e As EventArgs) Handles SearchTextBox.TextChanged
        Try

            logicobject.FilterShippingInstructionList(SearchTextBox.Text)
            ShippingInstructionDataGridView.DataSource = logicobject.ShippingInstructionList
        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ShippingInstructionDataGridView_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles ShippingInstructionDataGridView.CellDoubleClick, ShippingInstructionDataGridView.CellDoubleClick
        Try
            If ShippingInstructionDataGridView.SelectedRows.Count > 0 Then
                Dim Instruction = ShippingInstructionDataGridView.SelectedRows(0).Cells(0).Value.ToString()

                Dim InstructionSelect As New ShippingInstructions

                InstructionSelect.ShippingInstruction = Instruction



                logicobject.SelectedShippingInstruction = InstructionSelect

                Me.Close()

            End If

        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try

    End Sub
End Class