﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AddDispatchNoteForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AddDispatchNoteForm))
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.CustomerTextBox = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.DispatchCancelButton = New System.Windows.Forms.Button()
        Me.DispatchSaveButton = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.POTextBox = New System.Windows.Forms.TextBox()
        Me.NoOfLinesTextBox = New System.Windows.Forms.TextBox()
        Me.KGTextBox = New System.Windows.Forms.TextBox()
        Me.DispatchNoteListView = New System.Windows.Forms.ListView()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.ItemsNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.DispatchNoteTextBox = New System.Windows.Forms.TextBox()
        Me.ItemDescriptionComboBox = New System.Windows.Forms.ComboBox()
        Me.CubicUnitTextBox = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.WeightUnitTextBox = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.DimensionQuantityNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.DimensionLengthNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.DimensionWidthNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.DimensionHeightNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.ItemsNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.DimensionQuantityNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DimensionLengthNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DimensionWidthNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DimensionHeightNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.9453!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75.0547!))
        Me.TableLayoutPanel1.Controls.Add(Me.Label2, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.CustomerTextBox, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 1, 20)
        Me.TableLayoutPanel1.Controls.Add(Me.Label4, 0, 11)
        Me.TableLayoutPanel1.Controls.Add(Me.Label3, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Label12, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.Label13, 0, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.POTextBox, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.NoOfLinesTextBox, 1, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.KGTextBox, 1, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.DispatchNoteListView, 0, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.Label14, 0, 10)
        Me.TableLayoutPanel1.Controls.Add(Me.ItemsNumericUpDown, 1, 10)
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel3, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.ItemDescriptionComboBox, 1, 11)
        Me.TableLayoutPanel1.Controls.Add(Me.CubicUnitTextBox, 1, 12)
        Me.TableLayoutPanel1.Controls.Add(Me.Label6, 0, 12)
        Me.TableLayoutPanel1.Controls.Add(Me.Label7, 0, 13)
        Me.TableLayoutPanel1.Controls.Add(Me.WeightUnitTextBox, 1, 13)
        Me.TableLayoutPanel1.Controls.Add(Me.Label8, 0, 14)
        Me.TableLayoutPanel1.Controls.Add(Me.DimensionQuantityNumericUpDown, 1, 14)
        Me.TableLayoutPanel1.Controls.Add(Me.Label9, 0, 15)
        Me.TableLayoutPanel1.Controls.Add(Me.DimensionLengthNumericUpDown, 1, 15)
        Me.TableLayoutPanel1.Controls.Add(Me.Label10, 0, 16)
        Me.TableLayoutPanel1.Controls.Add(Me.DimensionWidthNumericUpDown, 1, 16)
        Me.TableLayoutPanel1.Controls.Add(Me.Label11, 0, 17)
        Me.TableLayoutPanel1.Controls.Add(Me.DimensionHeightNumericUpDown, 1, 17)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 23
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(457, 598)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 41)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Customer"
        '
        'CustomerTextBox
        '
        Me.CustomerTextBox.Location = New System.Drawing.Point(117, 44)
        Me.CustomerTextBox.Name = "CustomerTextBox"
        Me.CustomerTextBox.ReadOnly = True
        Me.CustomerTextBox.Size = New System.Drawing.Size(306, 20)
        Me.CustomerTextBox.TabIndex = 3
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.DispatchCancelButton)
        Me.Panel1.Controls.Add(Me.DispatchSaveButton)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(117, 495)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(337, 37)
        Me.Panel1.TabIndex = 24
        '
        'DispatchCancelButton
        '
        Me.DispatchCancelButton.Image = CType(resources.GetObject("DispatchCancelButton.Image"), System.Drawing.Image)
        Me.DispatchCancelButton.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.DispatchCancelButton.Location = New System.Drawing.Point(132, 7)
        Me.DispatchCancelButton.Name = "DispatchCancelButton"
        Me.DispatchCancelButton.Size = New System.Drawing.Size(75, 23)
        Me.DispatchCancelButton.TabIndex = 1
        Me.DispatchCancelButton.Text = "Cancel"
        Me.DispatchCancelButton.UseVisualStyleBackColor = True
        '
        'DispatchSaveButton
        '
        Me.DispatchSaveButton.Image = CType(resources.GetObject("DispatchSaveButton.Image"), System.Drawing.Image)
        Me.DispatchSaveButton.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.DispatchSaveButton.Location = New System.Drawing.Point(12, 7)
        Me.DispatchSaveButton.Name = "DispatchSaveButton"
        Me.DispatchSaveButton.Size = New System.Drawing.Size(75, 23)
        Me.DispatchSaveButton.TabIndex = 0
        Me.DispatchSaveButton.Text = "Save"
        Me.DispatchSaveButton.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(3, 267)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(60, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Description"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(3, 67)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(22, 13)
        Me.Label3.TabIndex = 26
        Me.Label3.Text = "PO"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(3, 93)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(63, 13)
        Me.Label12.TabIndex = 27
        Me.Label12.Text = "No Of Lines"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(3, 119)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(22, 13)
        Me.Label13.TabIndex = 28
        Me.Label13.Text = "KG"
        '
        'POTextBox
        '
        Me.POTextBox.Location = New System.Drawing.Point(117, 70)
        Me.POTextBox.Name = "POTextBox"
        Me.POTextBox.ReadOnly = True
        Me.POTextBox.Size = New System.Drawing.Size(306, 20)
        Me.POTextBox.TabIndex = 30
        '
        'NoOfLinesTextBox
        '
        Me.NoOfLinesTextBox.Location = New System.Drawing.Point(117, 96)
        Me.NoOfLinesTextBox.Name = "NoOfLinesTextBox"
        Me.NoOfLinesTextBox.ReadOnly = True
        Me.NoOfLinesTextBox.Size = New System.Drawing.Size(306, 20)
        Me.NoOfLinesTextBox.TabIndex = 31
        '
        'KGTextBox
        '
        Me.KGTextBox.Location = New System.Drawing.Point(117, 122)
        Me.KGTextBox.Name = "KGTextBox"
        Me.KGTextBox.ReadOnly = True
        Me.KGTextBox.Size = New System.Drawing.Size(306, 20)
        Me.KGTextBox.TabIndex = 32
        '
        'DispatchNoteListView
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.DispatchNoteListView, 2)
        Me.DispatchNoteListView.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DispatchNoteListView.Location = New System.Drawing.Point(3, 147)
        Me.DispatchNoteListView.Name = "DispatchNoteListView"
        Me.TableLayoutPanel1.SetRowSpan(Me.DispatchNoteListView, 4)
        Me.DispatchNoteListView.Size = New System.Drawing.Size(451, 92)
        Me.DispatchNoteListView.TabIndex = 25
        Me.DispatchNoteListView.UseCompatibleStateImageBehavior = False
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(3, 242)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(32, 13)
        Me.Label14.TabIndex = 29
        Me.Label14.Text = "Items"
        '
        'ItemsNumericUpDown
        '
        Me.ItemsNumericUpDown.Location = New System.Drawing.Point(117, 245)
        Me.ItemsNumericUpDown.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.ItemsNumericUpDown.Name = "ItemsNumericUpDown"
        Me.ItemsNumericUpDown.Size = New System.Drawing.Size(278, 20)
        Me.ItemsNumericUpDown.TabIndex = 35
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Dispatch Note"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.DispatchNoteTextBox)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(117, 9)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(337, 29)
        Me.Panel3.TabIndex = 37
        '
        'DispatchNoteTextBox
        '
        Me.DispatchNoteTextBox.Location = New System.Drawing.Point(0, 3)
        Me.DispatchNoteTextBox.Name = "DispatchNoteTextBox"
        Me.DispatchNoteTextBox.ReadOnly = True
        Me.DispatchNoteTextBox.Size = New System.Drawing.Size(306, 20)
        Me.DispatchNoteTextBox.TabIndex = 1
        '
        'ItemDescriptionComboBox
        '
        Me.ItemDescriptionComboBox.FormattingEnabled = True
        Me.ItemDescriptionComboBox.Items.AddRange(New Object() {"Pallet", "Carton", "Roll", "Bundle", "Gate", "Other"})
        Me.ItemDescriptionComboBox.Location = New System.Drawing.Point(117, 270)
        Me.ItemDescriptionComboBox.Name = "ItemDescriptionComboBox"
        Me.ItemDescriptionComboBox.Size = New System.Drawing.Size(278, 21)
        Me.ItemDescriptionComboBox.TabIndex = 39
        '
        'CubicUnitTextBox
        '
        Me.CubicUnitTextBox.Location = New System.Drawing.Point(117, 295)
        Me.CubicUnitTextBox.Name = "CubicUnitTextBox"
        Me.CubicUnitTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.CubicUnitTextBox.Size = New System.Drawing.Size(278, 20)
        Me.CubicUnitTextBox.TabIndex = 11
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(3, 292)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(56, 13)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Cubic Unit"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(3, 317)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(63, 13)
        Me.Label7.TabIndex = 14
        Me.Label7.Text = "Weight Unit"
        '
        'WeightUnitTextBox
        '
        Me.WeightUnitTextBox.Location = New System.Drawing.Point(117, 320)
        Me.WeightUnitTextBox.Name = "WeightUnitTextBox"
        Me.WeightUnitTextBox.Size = New System.Drawing.Size(278, 20)
        Me.WeightUnitTextBox.TabIndex = 15
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(3, 342)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(98, 13)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "Dimension Quantity"
        '
        'DimensionQuantityNumericUpDown
        '
        Me.DimensionQuantityNumericUpDown.Location = New System.Drawing.Point(117, 345)
        Me.DimensionQuantityNumericUpDown.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.DimensionQuantityNumericUpDown.Name = "DimensionQuantityNumericUpDown"
        Me.DimensionQuantityNumericUpDown.Size = New System.Drawing.Size(278, 20)
        Me.DimensionQuantityNumericUpDown.TabIndex = 17
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(3, 367)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(92, 13)
        Me.Label9.TabIndex = 18
        Me.Label9.Text = "Dimension Length"
        '
        'DimensionLengthNumericUpDown
        '
        Me.DimensionLengthNumericUpDown.DecimalPlaces = 2
        Me.DimensionLengthNumericUpDown.Location = New System.Drawing.Point(117, 370)
        Me.DimensionLengthNumericUpDown.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.DimensionLengthNumericUpDown.Name = "DimensionLengthNumericUpDown"
        Me.DimensionLengthNumericUpDown.Size = New System.Drawing.Size(278, 20)
        Me.DimensionLengthNumericUpDown.TabIndex = 19
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(3, 392)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(87, 13)
        Me.Label10.TabIndex = 21
        Me.Label10.Text = "Dimension Width"
        '
        'DimensionWidthNumericUpDown
        '
        Me.DimensionWidthNumericUpDown.DecimalPlaces = 2
        Me.DimensionWidthNumericUpDown.Location = New System.Drawing.Point(117, 395)
        Me.DimensionWidthNumericUpDown.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.DimensionWidthNumericUpDown.Name = "DimensionWidthNumericUpDown"
        Me.DimensionWidthNumericUpDown.Size = New System.Drawing.Size(278, 20)
        Me.DimensionWidthNumericUpDown.TabIndex = 20
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(3, 417)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(90, 13)
        Me.Label11.TabIndex = 23
        Me.Label11.Text = "Dimension Height"
        '
        'DimensionHeightNumericUpDown
        '
        Me.DimensionHeightNumericUpDown.DecimalPlaces = 2
        Me.DimensionHeightNumericUpDown.Location = New System.Drawing.Point(117, 420)
        Me.DimensionHeightNumericUpDown.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.DimensionHeightNumericUpDown.Name = "DimensionHeightNumericUpDown"
        Me.DimensionHeightNumericUpDown.Size = New System.Drawing.Size(278, 20)
        Me.DimensionHeightNumericUpDown.TabIndex = 22
        '
        'AddDispatchNoteForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(457, 598)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "AddDispatchNoteForm"
        Me.Text = "AddDispatchNoteForm"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.ItemsNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.DimensionQuantityNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DimensionLengthNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DimensionWidthNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DimensionHeightNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DispatchNoteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents CustomerTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents CubicUnitTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents WeightUnitTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents DimensionQuantityNumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents DimensionLengthNumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents DimensionWidthNumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents DimensionHeightNumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents DispatchCancelButton As System.Windows.Forms.Button
    Friend WithEvents DispatchSaveButton As System.Windows.Forms.Button
    Friend WithEvents DispatchNoteListView As System.Windows.Forms.ListView
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents POTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NoOfLinesTextBox As System.Windows.Forms.TextBox
    Friend WithEvents KGTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ItemsNumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents ItemDescriptionComboBox As System.Windows.Forms.ComboBox
End Class
